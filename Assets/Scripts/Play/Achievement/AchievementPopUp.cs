﻿using System;
using System.Collections.Generic;
using System.Linq;
using Harmony;
using TMPro;
using UnityEngine;

namespace Game
{
    public class AchievementPopUp : MonoBehaviour
    {
        
        [SerializeField] private AnimationClip animation;
        [SerializeField] private TextMeshProUGUI achievementDescription;

        private Animator animator;
        private List<Achievement> achievements;
        private AchievementObtainedEventChannel achievementObtainedEventChannel;

        private bool IsAnimationRunning => animator.GetCurrentAnimatorStateInfo(0).IsName(animation.name);

        private void Awake()
        {
            animator = GetComponent<Animator>();
            achievements = new List<Achievement>();
            achievementObtainedEventChannel = Finder.AchievementObtainedEventChannel;
        }

        private void Start()
        {
            achievementObtainedEventChannel.OnAchievementGained += OnAchievementGained;
        }

        private void Update()
        {
            if (achievements.Any() && !IsAnimationRunning)
            {
                animator.Play(animation.name);
                
                changeDescription(achievements.First());
                achievements.RemoveAt(0);
            }
        }

        private void OnDestroy()
        {
            achievementObtainedEventChannel.OnAchievementGained -= OnAchievementGained;
        }

        private void OnAchievementGained(Achievement achievement)
        {
            achievements.Add(achievement);
        }

        private void changeDescription(Achievement achievement)
        {
            String textAchievement = "";
            switch (achievement)
            {
                case Achievement.Level1Finished:
                    textAchievement = AchievementName.Level1Completed;
                    break;
                case Achievement.Level2Finished:
                    textAchievement = AchievementName.Level2Completed;
                    break;
                case Achievement.Level3Finished:
                    textAchievement = AchievementName.Level3Completed;
                    break;
                case Achievement.Level4Finished:
                    textAchievement = AchievementName.Level4Completed;
                    break;
                case Achievement.Level5Finished:
                    textAchievement = AchievementName.Level5Completed;
                    break;
                case Achievement.Level6Finished:
                    textAchievement = AchievementName.Level6Completed;
                    break;
                case Achievement.Backfire:
                    textAchievement = AchievementName.Backfire;
                    break;
                case Achievement.Desactivated:
                    textAchievement = AchievementName.Desactivated;
                    break;
                case Achievement.Overclocking:
                    textAchievement = AchievementName.Overclocking;
                    break;
                case Achievement.Speedrunner:
                    textAchievement = AchievementName.Speedrunner;
                    break;
                case Achievement.Traveler:
                    textAchievement = AchievementName.Traveler;
                    break;
                case Achievement.Trapped:
                    textAchievement = AchievementName.Trapped;
                    break;
                case Achievement.Hard:
                    textAchievement = AchievementName.Hard;
                    break;
                case Achievement.KilledBoss:
                    textAchievement = AchievementName.KilledBoss;
                    break;
            }
            achievementDescription.text =  "Vous avez obtenue le trophée \"" + textAchievement + "\"!";
        }
    }
}