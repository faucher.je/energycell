﻿namespace Game
{
    public enum Achievement
    {
        Level1Finished,
        Level2Finished,
        Level3Finished,
        Level4Finished,
        Level5Finished,
        Level6Finished,
        Traveler,
        KilledBoss,
        Desactivated,
        Hard,
        Overclocking,
        Backfire,
        Speedrunner,
        Trapped
    }
}