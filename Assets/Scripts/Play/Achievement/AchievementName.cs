﻿using Harmony;

namespace Game
{
    public static class AchievementName
    {
        public const string Level1Completed = "Niveau 1 Complété";
        public const string Level2Completed = "Niveau 2 Complété";
        public const string Level3Completed = "Niveau 3 Complété";
        public const string Level4Completed = "Niveau 4 Complété";
        public const string Level5Completed = "Niveau 5 Complété";
        public const string Level6Completed = "Niveau 6 Complété";
        public const string Traveler = "Voyageur";
        public const string KilledBoss = "Professeur hors service";
        public const string Desactivated = "Désactivation";
        public const string Speedrunner = "Speedrunner";
        public const string Backfire = "Retour de flamme";
        public const string Overclocking = "Overclocking";
        public const string Hard = "C\'est difficile!";
        public const string Trapped = "You have trapped yourself. Congratulations.";
    }
}