﻿using System;
using System.Collections;
using Harmony;
using UnityEngine;

namespace Game
{
    //Author : Samuel Ménard
    [Findable(Tags.MainController)]
    public class AchievementsCollector : MonoBehaviour
    {
        [SerializeField] private int numberOfDeathForAchievement = 10;
        [SerializeField] private int timeInOtherDimensionForVoyageurAchievement = 20;
        [SerializeField] private int timeLimitInMinuteForSpeedrunnerAchievement = 15;

        private int numberOfDeath = 0;
        private bool hasReachedMaxHeat = false;
        private bool stayedLongEnoughInOtherDimension = false;
        private bool[] isLevelsFinished;
        private bool hasKilledFleshEnemy = false;
        private bool hasKilledTurret = false;
        private bool hasKilledBoss = false;
        private bool hasFinishedGameUnderTimeLimit = false;
        private bool underSpeedrunnerTimeLimit = false;
        private PlayerHeatChangeEventChannel heatChangeEventChannel;
        private PlayerHealthChangeEventChannel healthChangeEventChannel;
        private DimensionChangeEventChannel dimensionChangeEventChannel;
        private LevelEndEventChannel levelEndEventChannel;
        private EnemyDeathEventChannel enemyDeathEventChannel;
        private AchievementObtainedEventChannel achievementObtainedEventChannel;
        private AchievementSpeedrunnerEventChannel achievementSpeedrunnerEventChannel;
        private Coroutine timerForSpeedrunnerAchievementCoroutine;
        private Coroutine timerForVoyageurAchievementCoroutine;
        private Player player;

        public bool HasReachedMaxHeat
        {
            get => hasReachedMaxHeat;
            private set
            {
                hasReachedMaxHeat = value;
                achievementObtainedEventChannel.Publish(Achievement.Overclocking);
            }
        }

        public int NumberOfDeath
        {
            get => numberOfDeath;
            private set
            {
                numberOfDeath = value;
                
                if (numberOfDeath == numberOfDeathForAchievement)
                {
                    achievementObtainedEventChannel.Publish(Achievement.Hard);
                    HasDiedEnoughForAchievement = true;
                }
            }
        }

        public bool HasDiedEnoughForAchievement { get; private set; }

        public bool StayedLongEnoughInOtherDimension
        {
            get => stayedLongEnoughInOtherDimension;
            private set
            {
                stayedLongEnoughInOtherDimension = value;
                achievementObtainedEventChannel.Publish(Achievement.Traveler);
            }
        }

        public bool[] IsLevelsFinished
        {
            get => isLevelsFinished;
        }
        
        public bool HasKilledFleshEnemy
        {
            get => hasKilledFleshEnemy;
            private set
            {
                hasKilledFleshEnemy = value;
                achievementObtainedEventChannel.Publish(Achievement.Backfire);
            } 
        }

        public bool HasKilledBoss
        {
            get => hasKilledBoss;
            private set
            {
                hasKilledBoss = value;
                achievementObtainedEventChannel.Publish(Achievement.KilledBoss);
            }
        }

        public bool HasKilledTurret
        {
            get => hasKilledTurret;
            private set
            {
                hasKilledTurret = value;
                achievementObtainedEventChannel.Publish(Achievement.Desactivated);
            } 
        }
        
        public bool HasFinishedGameUnderTimeLimit
        {
            get => hasFinishedGameUnderTimeLimit;
            private set
            {
                hasFinishedGameUnderTimeLimit = value;
                achievementObtainedEventChannel.Publish(Achievement.Speedrunner);
            }
        }

        private void Awake()
        {
            //It's to get the exact number of level without hardcoding it.
            isLevelsFinished = new bool[LevelController.NumberOfLevel];

            for (int i = 0; i < isLevelsFinished.Length; i++)
                isLevelsFinished[i] = false;

            heatChangeEventChannel = GetComponent<PlayerHeatChangeEventChannel>();
            healthChangeEventChannel = GetComponent<PlayerHealthChangeEventChannel>();
            dimensionChangeEventChannel = GetComponent<DimensionChangeEventChannel>();
            levelEndEventChannel = GetComponent<LevelEndEventChannel>();
            enemyDeathEventChannel = GetComponent<EnemyDeathEventChannel>();
            achievementSpeedrunnerEventChannel = GetComponent<AchievementSpeedrunnerEventChannel>();
            achievementObtainedEventChannel = GetComponent<AchievementObtainedEventChannel>();
        }

        private void Start()
        {
            heatChangeEventChannel.OnPlayerHeatChange += OnHeatChanged;
            healthChangeEventChannel.OnPlayerHealthChanged += OnHealthChanged;
            dimensionChangeEventChannel.OnDimensionChange += OnDimensionChanged;
            levelEndEventChannel.OnLevelEnd += OnLevelChanged;
            enemyDeathEventChannel.OnEnemyDeath += OnEnemyDeath;
            achievementSpeedrunnerEventChannel.OnAchievementSpeedrunnerStateChanged +=
                OnAchievementSpeedrunnerStateChanged;
        }

        private void OnDestroy()
        {
            heatChangeEventChannel.OnPlayerHeatChange -= OnHeatChanged;
            healthChangeEventChannel.OnPlayerHealthChanged -= OnHealthChanged;
            dimensionChangeEventChannel.OnDimensionChange -= OnDimensionChanged;
            levelEndEventChannel.OnLevelEnd -= OnLevelChanged;
            enemyDeathEventChannel.OnEnemyDeath -= OnEnemyDeath;
            achievementSpeedrunnerEventChannel.OnAchievementSpeedrunnerStateChanged -=
                OnAchievementSpeedrunnerStateChanged;
        }

        private void OnHeatChanged(float currentHeat)
        {
            //Temporary code until I fixed it (Probs either adding event channel after a level is loaded).
            if (player == null) player = Finder.Player;

            if (!hasReachedMaxHeat && currentHeat == player.Vitals.MaxHeatPoints)
                HasReachedMaxHeat = true;
        }

        private void OnHealthChanged(float health)
        {
            //Temporary code until I fixed it (Probs either adding event channel after a level is loaded).
            if (player == null) player = Finder.Player;

            if (health == player.Vitals.MinHealthPoints)
                NumberOfDeath += 1;
        }

        private void OnDimensionChanged(Dimension dimension)
        {
            if (!StayedLongEnoughInOtherDimension)
                switch (dimension)
                {
                    case Dimension.First:
                        if (timerForVoyageurAchievementCoroutine != null)
                            StopCoroutine(timerForVoyageurAchievementCoroutine);
                        break;
                    case Dimension.Second:
                        timerForVoyageurAchievementCoroutine = StartCoroutine(TimerForVoyageurAchievement());
                        break;
                }
        }

        private IEnumerator TimerForVoyageurAchievement()
        {
            yield return new WaitForSeconds(timeInOtherDimensionForVoyageurAchievement);
            StayedLongEnoughInOtherDimension = true;
        }

        private void OnLevelChanged(Level level)
        {
            // Due to the fact level in int start at 0, we add 1 to it to get the level number.
            int levelNumber = (int) level + 1;
            if (!IsLevelsFinished[(int)level])    
            {
                IsLevelsFinished[(int)level] = true;
                //It wasn't done in set because it's an array.
                string achievementText = Achievement.Level1Finished.ToString().Replace('1', levelNumber.ToString()[0]);
                achievementObtainedEventChannel.Publish((Achievement) Enum.Parse(typeof(Achievement),achievementText, true));
            }

            if (level == Level.Level6 && underSpeedrunnerTimeLimit)
                HasFinishedGameUnderTimeLimit = true;
        }


        private void OnEnemyDeath(EnemyType enemyType)
        {
            if (!HasKilledTurret && enemyType == EnemyType.SyntheticEnemy)
                HasKilledTurret = true;
            else if (!HasKilledFleshEnemy && enemyType == EnemyType.FleshEnemy) 
                HasKilledFleshEnemy = true;
            else if (!hasKilledBoss && enemyType == EnemyType.Boss)
                HasKilledBoss = true;
        }

        private void OnAchievementSpeedrunnerStateChanged(bool isPlayerDoingAchievement)
        {
            if (!HasFinishedGameUnderTimeLimit)
            {
                switch (isPlayerDoingAchievement)
                {
                    case true:
                        timerForSpeedrunnerAchievementCoroutine = StartCoroutine(TimerForSpeedrunnerAchievement());
                        break;
                    case false:
                        if(timerForSpeedrunnerAchievementCoroutine != null)
                            StopCoroutine(TimerForSpeedrunnerAchievement());
                        break;
                }
            }
        }

        private IEnumerator TimerForSpeedrunnerAchievement()
        {
            underSpeedrunnerTimeLimit = true;
            yield return new WaitForSeconds(timeLimitInMinuteForSpeedrunnerAchievement * 60);
            underSpeedrunnerTimeLimit = false;
        }
        
    }
}