﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Mathieu Boutet
    public static class Vector2Extensions
    {
        /// <summary>
        /// Tells if the point is before another point in the specified direction.
        /// </summary>
        public static bool IsBefore(this Vector2 point1, Vector2 point2, Vector2 direction, float precision = 1f)
        {
            return point2.IsPast(point1, direction, precision);
        }
        
        /// <summary>
        /// Tells if the point is past another point in the specified direction.
        /// </summary>
        public static bool IsPast(this Vector2 point1, Vector2 point2, Vector2 direction, float precision = 1f)
        {
            return point2.DirectionTo(point1).IsSameDirection(direction, precision);
        }


        /// <summary>
        /// Tells if the point is visible to the specified camera.
        /// </summary>
        public static bool IsVisible(this Vector2 point, Camera camera)
        {
            var viewPortPosition = camera.WorldToViewportPoint(point);
            return viewPortPosition.x > 0 && viewPortPosition.x < 1
                                          && viewPortPosition.y > 0 && viewPortPosition.y < 1;
        }
    }
}