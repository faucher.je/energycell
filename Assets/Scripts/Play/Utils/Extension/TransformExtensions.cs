﻿using UnityEngine;

namespace Game
{
    // Author : Mathieu Boutet
    public static class TransformExtensions
    {
        // https://forum.unity.com/threads/find-local-rotation-given-global-rotation.136379/#post-926556
        public static Quaternion WorldToLocalRotation(this Transform transform, Quaternion worldRotation)
        {
            var originalRotation = transform.rotation;
            transform.rotation = worldRotation;
            var localRotation = transform.localRotation;
            transform.rotation = originalRotation;
            return localRotation;
        }
        
        public static Quaternion LocalToWorldRotation(this Transform transform, Quaternion localRotation)
        {
            var originalRotation = transform.rotation;
            transform.localRotation = localRotation;
            var worldRotation = transform.rotation;
            transform.rotation = originalRotation;
            return worldRotation;
        }
    }
}