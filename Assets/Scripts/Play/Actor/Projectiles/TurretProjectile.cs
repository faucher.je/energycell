﻿using System;
using System.Collections;
using UnityEngine;

namespace Game
{
    // Author : Mathieu Boutet
    public class TurretProjectile : EnemyProjectile
    {
        public Turret Turret { get; set; }
        public Rigidbody2D Rigidbody2D { get; private set; }

        private void Awake()
        {
            Rigidbody2D = GetComponent<Rigidbody2D>();
            OriginalPosition = transform.position;
        }

        private void OnEnable()
        {
            StartCoroutine(LifetimeCoroutine());
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            //Temporary code so bullet can hurt player.
            GameObject collisionGameObject = other.gameObject;

            if (other.transform.IsChildOf(Turret.transform)) return;

            PlayerVitals playerVitals = collisionGameObject.GetComponent<PlayerVitals>();
            if(playerVitals != null) playerVitals.ReceiveDamage(Damage);
            
            Remove();
        }

        protected override void Remove()
        {
            StopCoroutine(LifetimeCoroutine());
            Turret.RemoveProjectile(this);
        }
        
        private IEnumerator LifetimeCoroutine()
        {
            yield return new WaitForSeconds(Turret.ProjectileMaxLifetime);
            Turret.RemoveProjectile(this);
        }
    }
}