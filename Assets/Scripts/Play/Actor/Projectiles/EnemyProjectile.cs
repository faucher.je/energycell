using UnityEngine;

namespace Game
{
    // Author : Samuel Ménard
    public class EnemyProjectile : Projectile
    {
        [SerializeField] private float damage;

        protected float Damage => damage;

        private void OnCollisionEnter2D(Collision2D collision)
        {
            //Temporary code so bullet can hurt player.
            GameObject collisionGameObject = collision.gameObject;

            PlayerVitals playerVitals = collisionGameObject.GetComponent<PlayerVitals>();
            
            if(playerVitals != null) playerVitals.ReceiveDamage(Damage);
            
            Remove();
        }

        protected virtual void Remove()
        {
            Destroy(gameObject);
        }
    }
}