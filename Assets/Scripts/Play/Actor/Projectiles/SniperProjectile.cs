﻿using UnityEngine;

namespace Game
{
    //Author: Rebecca Brassard
    public class SniperProjectile : EnemyProjectile
    {
        private void Update()
        {
            if (Vector3.Distance(transform.position, OriginalPosition) >= MaxDistance) Destroy(gameObject);
        }
    }
}