﻿using UnityEngine;

namespace Game
{
    // Author : Samuel Ménard
    public abstract class Projectile : MonoBehaviour
    {
        [SerializeField] private float maxDistance = 25f;

        protected float MaxDistance => maxDistance;

        protected Vector3 OriginalPosition { get; set; }
        
        public void OnEnable()
        {
            OriginalPosition = transform.position;
        }
    }
}