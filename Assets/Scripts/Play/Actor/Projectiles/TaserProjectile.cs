﻿using Harmony;
using UnityEngine;

namespace Game
{
    //Author : Samuel Ménard
    public class TaserProjectile : Projectile
    {
        [SerializeField] private float projectileSpeed = 10;
        
        private BoxCollider2D playerBoxCollider;
        private BoxCollider2D bulletBoxCollider;

        private Rigidbody2D bulletBody;
        private PlayerAim playerAim;

        private void Awake()
        {
            playerBoxCollider = Finder.Player.Collider2D;
            playerAim = Finder.PlayerAim;
            bulletBoxCollider = GetComponent<BoxCollider2D>();
            bulletBody = GetComponent<Rigidbody2D>();
            gameObject.SetActive(false);
        }

        private void OnEnable()
        {
            transform.position = playerAim.transform.position;
            bulletBody.velocity = playerAim.AimingDirection * projectileSpeed;
            //It's necessary to ignore the collider because if it collides, the bullet will disappear and even if we ignore
            //the collision in another way, the body of the player will be slowed down.
            if (playerBoxCollider == null) playerBoxCollider = Finder.Player.Collider2D;
            Physics2D.IgnoreCollision(bulletBoxCollider, playerBoxCollider);
            OriginalPosition = transform.position;
            base.OnEnable();
        }

        private void Update()
        {
            if (Vector2.Distance(transform.position, OriginalPosition) >= MaxDistance) gameObject.SetActive(false);
        }
        
        private void OnCollisionEnter2D(Collision2D collision)
        {
            GameObject collisionGameObject = collision.gameObject;

            ITaseable taseable = collisionGameObject.GetComponentInParent<ITaseable>();

            if (taseable != null) taseable.Tase();
            
            gameObject.SetActive(false);
        }
    }
}