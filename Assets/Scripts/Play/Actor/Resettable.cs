﻿using UnityEngine;

namespace Game
{
    // Author: Jérémy Faucher
    // Tout élément devant être réinitialisé lorsque le joueur meurt doit hériter de cette classe
    // et override les méthodes présentes ici afin de pouvoir réinitialiser à l'état original.
    // Voir la classe Enemy pour un exemple
    public class Resettable : MonoBehaviour
    {
        private Vector3 origPosition;

        protected virtual void Awake()
        {
            origPosition = transform.position;
        }

        public virtual void ResetToOriginalState()
        {
            transform.position = origPosition;
        }
    }
}