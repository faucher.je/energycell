﻿using System.Collections;
using Harmony;
using UnityEngine;

//Author: Rebecca Brassard
namespace Game
{
    public class Sniper : MovableEnemy
    {
        [SerializeField] private GameObject projectile;
        [SerializeField] private float timeToShoot = 0.75f;
        [SerializeField] private float projectileSpeed = 1.5f;
        private bool isShooting = false;
        private Animator animator;

        protected override void Awake()
        {
            base.Awake();
            animator = GetComponentInChildren<Animator>();
        }

        protected override void FixedUpdate()
        {
            if (TargetSpotted)
            {
                animator.SetBool(AnimatorParameters.Walk, false);
                if (!isShooting)
                {
                    PrepareShot();
                }
            }
            else
            {
                animator.SetBool(AnimatorParameters.Walk, true);
                base.FixedUpdate();
            }
        }

        private void PrepareShot()
        {
            isShooting = true;
            Vector2 aim = GetVectorBetweenSniperAndPlayer();
            aim = aim.normalized;
            IEnumerator coroutine = Shoot(aim);
            StartCoroutine(coroutine);
        }

        private Vector2 GetVectorBetweenSniperAndPlayer()
        {
            return LastKnownTargetPosition - transform.position;
        }

        private IEnumerator Shoot(Vector2 aim)
        {
            yield return new WaitForSeconds(timeToShoot);
            isShooting = false;
            GameObject shotProjectile = Instantiate(projectile, transform.position, Quaternion.identity);
            shotProjectile.GetComponent<Rigidbody2D>().velocity = aim * projectileSpeed;
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.collider.CompareTag(Tags.Player))
            {
                animator.SetTrigger(AnimatorParameters.Slash);
                Target.Knockback(transform.position);
                Target.Hurt(DamageDealtToPlayerOnHit);
            }
        }
    }
}