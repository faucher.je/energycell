﻿using System;
using UnityEngine;

namespace Game
{
    //Author: Rebecca Brassard
    [RequireComponent(typeof(Rigidbody2D))]
    public abstract class MovableEnemy : Enemy
    {
        [SerializeField] private float walkSpeed = 3f;
        [SerializeField] private float[] waypointsInXAxis;

        protected new Rigidbody2D rigidbody2D;
        private int currentWaypoint = 0;
        protected bool isLookingRight = true;

        protected bool IsLookingRight
        {
            get => isLookingRight;
            set
            {
                if (value)
                {
                    Quaternion transformRotation = transform.rotation;
                    transformRotation.y = 0;
                    transform.rotation = transformRotation;
                }
                else
                {
                    Quaternion transformRotation = transform.rotation;
                    transformRotation.y = 180;
                    transform.rotation = transformRotation;
                }

                isLookingRight = value;
            }
        }

        protected float WalkSpeed => walkSpeed;

        protected override void Awake()
        {
            base.Awake();
            EnemyType = EnemyType.FleshEnemy;
            rigidbody2D = GetComponent<Rigidbody2D>();
        }

        protected virtual void Update()
        {
            SearchForTarget();
        }

        protected virtual void FixedUpdate()
        {
            MoveToNextWaypoints();
        }

        protected void MoveToNextWaypoints()
        {
            if (Math.Abs(waypointsInXAxis[currentWaypoint] - transform.position.x) <= 0.3f)
            {
                rigidbody2D.velocity = new Vector2(0, rigidbody2D.velocity.y);
                currentWaypoint++;
                if (currentWaypoint == waypointsInXAxis.Length)
                {
                    currentWaypoint = 0;
                }
            }
            else if (waypointsInXAxis[currentWaypoint] > transform.position.x)
            {
                MoveRight(WalkSpeed);
            }
            else
            {
                MoveLeft(WalkSpeed);
            }
        }

        protected void MoveLeft(float speed)
        {
            rigidbody2D.velocity = new Vector2(-speed, rigidbody2D.velocity.y);
            if (IsLookingRight)
            {
                IsLookingRight = false;
            }
        }
        protected void MoveRight(float speed)
        {
            rigidbody2D.velocity = new Vector2(speed, rigidbody2D.velocity.y);
            if (!IsLookingRight)
            {
                IsLookingRight = true;
            }
        }
        
        // Author: Jérémy Faucher
        public override void Kill()
        {
            EnemyDeathEventChannel.Publish(EnemyType);
            gameObject.SetActive(false);
        }
    }
}