﻿using System;
using System.Collections;
using Harmony;
using UnityEngine;

namespace Game
{
    [RequireComponent(typeof(BoxCollider2D))]
    public abstract class Enemy : Resettable, ICharacter, ITaseable
    {
        [SerializeField] private float paralyzedTime = 5f;
        [SerializeField] private int damageDealtToPlayerOnHit = 20;

        [Header("Detection")] [SerializeField] [Tooltip("The object where the enemy sees from")]
        private Transform headTransform = null;

        [SerializeField] private float detectionRange = 10f;
        [SerializeField] private LayerMask visionBlockingLayers = default;
        
        private Coroutine currentParalyzedCoroutine;
        private new Camera camera;
        private bool targetSpotted;

        public Transform HeadTransform => headTransform;
        protected Vector3 LastKnownTargetPosition { get; set; }
        protected Player Target { get; private set; }
        protected float DetectionRange => detectionRange;
        protected int DamageDealtToPlayerOnHit => damageDealtToPlayerOnHit;
        protected EnemyType EnemyType { get; set; }

        // Author : Mathieu Boutet
        public virtual bool TargetSpotted
        {
            get => targetSpotted;
            protected set
            {
                if (value)
                    LastKnownTargetPosition = Target.transform.position;

                targetSpotted = value;
            }
        }

        // Author: Jérémy Faucher & Mathieu Boutet
        private bool TargetIsInRange
        {
            get
            {
                Vector2 position = transform.position;
                return Vector2.Distance(position, Target.transform.position) <= DetectionRange
                       && position.IsVisible(camera);
            }
        }

        private bool TargetIsVisible
        {
            get
            {
                var position = HeadTransform.position;
                var targetPosition = Target.transform.position;
                var hit = Physics2D.Raycast(
                    position,
                    position.DirectionTo(targetPosition),
                    Vector2.Distance(position, targetPosition),
                    visionBlockingLayers
                );

                return hit.collider == null;
            }
        }
        
        protected EnemyDeathEventChannel EnemyDeathEventChannel { set; get; }

        // Author: Jérémy Faucher
        protected override void Awake()
        {
            base.Awake();
            camera = Camera.main;
            Target = Finder.Player;
            LastKnownTargetPosition = Vector3.zero;
            EnemyDeathEventChannel = Finder.EnemyDeathEventChannel;
        }

        // Author : Mathieu Boutet
        protected void SearchForTarget() => TargetSpotted = TargetIsInRange && TargetIsVisible;

        // Author: Jérémy Faucher
        [ContextMenu("Tase")]
        public virtual void Tase()
        {
            if (currentParalyzedCoroutine != null) StopCoroutine(currentParalyzedCoroutine);
            currentParalyzedCoroutine = StartCoroutine(Paralyzed());
        }

        // Author: Jérémy Faucher
        private IEnumerator Paralyzed()
        {
            //Update ne peut plus être appelé, mais OnTriggerEnter2D peut quand même appelé
            enabled = false;
            yield return new WaitForSeconds(paralyzedTime);
            enabled = true;
            currentParalyzedCoroutine = null;
        }

        // Author: Jérémy Faucher
        public override void ResetToOriginalState()
        {
            base.ResetToOriginalState();
            LastKnownTargetPosition = Vector3.zero;
            gameObject.SetActive(true);
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.collider.CompareTag(Tags.Player))
            {
                Target.Hurt(DamageDealtToPlayerOnHit);
            }
        }

        public void Hurt(float damageReceived)
        {
            //TODO: enemy takes damage
        }

        public void Knockback(Vector2 otherPosition)
        {
            //TODO: enemy takes knockback
        }

        // Author : Mathieu Boutet
        [ContextMenu("Kill")]
        public abstract void Kill();
    }
}