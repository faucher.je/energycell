﻿using Harmony;
using UnityEngine;

namespace Game
{
    //Author: Jérémy Faucher
    [Findable(Tags.Boss)]
    [RequireComponent(typeof(BossPhaseController))]
    [RequireComponent(typeof(BossVitals))]
    public class Boss : Resettable
    {
        private const int ROTATION_TO_FLIP = 180;
        private Player player;
        
        public BossVitals BossVitals { get; private set; }
        public BossPhaseController BossPhaseController { get; private set; }

        private int YRotation => player.gameObject.transform.position.x > transform.position.x ? 0 : ROTATION_TO_FLIP;

        private void Awake()
        {
            player = Finder.Player;
            BossVitals = GetComponent<BossVitals>();
            BossPhaseController = GetComponent<BossPhaseController>();
        }

        private void Update()
        {
            FollowPlayer();
        }

        private void FollowPlayer()
        {
            transform.rotation = Quaternion.Euler(0, YRotation, 0);
        }
        
        public override void ResetToOriginalState()
        {
            BossVitals.ResetToOriginalState();
            BossPhaseController.ResetToOriginalState();
            FollowPlayer();
        }
    }
}