﻿namespace Game
{
    //Author: Jérémy Faucher
    public class BossStateMachine
    {
        private IState currentState;
        private BossPhaseController bossPhaseController;

        public BossStateMachine(BossPhaseController bossPhaseController)
        {
            this.bossPhaseController = bossPhaseController;
            currentState = new BossTurrets(bossPhaseController);
            currentState.Enter();
        }
        
        public void Update()
        {
            IState nextState = currentState.Update();

            if (nextState != currentState)
            {
                currentState.Leave();
                currentState = nextState;
                currentState.Enter();
            }
        }

        public void Reset()
        {
            currentState = new BossTurrets(bossPhaseController);
            currentState.Enter();
        }
    }
}