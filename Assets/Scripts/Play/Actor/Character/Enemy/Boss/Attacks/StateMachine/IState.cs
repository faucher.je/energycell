﻿namespace Game
{
    //Author: Jérémy Faucher
    public interface IState
    {
        void Enter();
        IState Update();
        void Leave();
    }
}