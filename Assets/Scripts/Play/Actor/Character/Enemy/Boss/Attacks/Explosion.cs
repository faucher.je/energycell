﻿using Harmony;
using UnityEngine;

namespace Game
{
    //Author: Jérémy Faucher
    [Findable(Tags.Explosion)]
    [RequireComponent(typeof(CircleCollider2D))]
    public class Explosion : MonoBehaviour
    {
        [SerializeField] private float heatAmount = 100f;
        [SerializeField] private GameObject summoner;

        public Vector3 Scale
        {
            get { return transform.localScale; }
            set { transform.localScale = value; }
        }

        private Player player;

        private void Awake()
        {
            player = Finder.Player;
            transform.position = summoner.transform.position;
            
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag(Tags.Player))
            {
                player.HeatUp(heatAmount);
            }
        }
    }
}