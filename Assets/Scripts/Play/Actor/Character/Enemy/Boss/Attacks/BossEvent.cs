﻿namespace Game
{
    //Author: Jérémy Faucher
    public enum BossEvent
    {
        PhaseIsOver,
        PhaseIsNotOver
    }
}