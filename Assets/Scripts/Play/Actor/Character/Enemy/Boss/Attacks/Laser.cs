﻿using Harmony;
using UnityEngine;

namespace Game
{
    //Author: Jérémy Faucher
    [RequireComponent(typeof(BoxCollider2D))]
    [RequireComponent(typeof(SpriteRenderer))]
    public class Laser : MonoBehaviour
    {
        [SerializeField] private float damage = 10f;
        
        private Player player;
        
        public BoxCollider2D BoxCollider2D { get; private set; }
        public SpriteRenderer SpriteRenderer { get; private set; }

        private void Awake()
        {
            player = Finder.Player;

            BoxCollider2D = GetComponent<BoxCollider2D>();
            SpriteRenderer = GetComponent<SpriteRenderer>();
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag(Tags.Player))
            {
                player.Hurt(damage);
            }
        }
    }
}