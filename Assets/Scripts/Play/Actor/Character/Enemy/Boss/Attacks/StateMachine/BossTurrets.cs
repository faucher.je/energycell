﻿
namespace Game
{
    //Author: Jérémy Faucher
    public class BossTurrets : IState
    {
        private BossPhaseController bossPhaseController;

        public BossTurrets(BossPhaseController bossPhaseController)
        {
            this.bossPhaseController = bossPhaseController;
        }

        public void Enter()
        {
            bossPhaseController.StartTurretAttack();
        }

        public IState Update()
        {
            var bossEvent = bossPhaseController.Read();
            switch (bossEvent)
            {
                case BossEvent.PhaseIsOver:
                    return new BossLaser(bossPhaseController);
                default:
                    return this;
            }
        }

        public void Leave()
        {
            bossPhaseController.ChangeActiveTurrets(false);
        }
    }
}