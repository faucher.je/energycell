﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Harmony;
using UnityEngine;

namespace Game
{
    //Author: Jérémy Faucher
    public class BossPhaseController : MonoBehaviour
    {
        [Header("All phases")]
        [SerializeField] private float alertDelay = 3f;
        
        [Header("Lasers")]
        [Range(0,1)] [SerializeField] private float warningOpacity = 0.5f;
        [SerializeField] private float laserEffectDelay = 0.25f;
        [SerializeField] private float laserOpacityEffect = 0.0625f;

        [Header("Turrets")]
        [SerializeField] private float zTurretIdleAim = 90f;

        [Header("Explosion")]
        [SerializeField] private float maximumExplosionScale = 15f;
        [SerializeField] private float explosionScaleChange = 0.5f;
        [SerializeField] private float explosionScaleChangeDelay = 0.0625f;
        [SerializeField] private Color alertAcolor = Color.red;

        [Header("Stun")] 
        [SerializeField] private float stunTime = 10f;

        // L'opacité maximale des lasers
        private const float FULL_OPACITY = 1f;
        
        // Pour la state machine
        private BossStateMachine stateMachine;
        private Stack<BossEvent> bossEvents;
        private bool phaseIsOver;
        
        // Pour la phase de tourelle
        private Turret[] turrets;
        
        // Pour la phase de l'explosion
        private Color origColor;
        private SpriteRenderer[] bossSpriteRenderers;
        private Explosion explosion;
        
        // Pour la phase des lasers
        private Laser[] lasers;
        
        // Pour la phase où le boss est stun
        private Prisonner[] prisonners;
        private ButtonTrigger[] buttonTriggers;
        
        // Pour toute les phases
        private Coroutine currentPhaseCoroutine;
        
        private void Awake()
        {
            turrets = FindObjectsOfType<Turret>();
        
            var tempLasers = GameObject.FindGameObjectsWithTag(Tags.Laser);
            lasers = new Laser[tempLasers.Length];
            for (int i = 0; i < tempLasers.Length; i++)
            {
                lasers[i] = tempLasers[i].gameObject.GetComponent<Laser>();
            }

            explosion = Finder.Explosion;
            
            var tempPrisonners = GameObject.FindGameObjectsWithTag(Tags.Ally);
            prisonners = new Prisonner[tempPrisonners.Length];
            for (int i = 0; i < tempPrisonners.Length; i++)
            {
                prisonners[i] = tempPrisonners[i].GetComponent<Prisonner>();
            }

            var tempButtons = GameObject.FindGameObjectsWithTag(Tags.Button);
            buttonTriggers = new ButtonTrigger[tempButtons.Length];
            for (int i = 0; i < tempButtons.Length; i++)
            {
                buttonTriggers[i] = tempButtons[i].GetComponent<ButtonTrigger>();
            }

            bossEvents = new Stack<BossEvent>();
            phaseIsOver = false;
            bossSpriteRenderers = GetComponentsInChildren<SpriteRenderer>();
            origColor = bossSpriteRenderers[0].color;

            DeactivateAllPhases();
            
            stateMachine = new BossStateMachine(this);
        }

        private void Update()
        {
            UpdateEventStack();
            stateMachine.Update();
        }

        public BossEvent Read()
        {
            return bossEvents.Pop();
        }
        
        private void UpdateEventStack()
        {
            if (phaseIsOver) bossEvents.Push(BossEvent.PhaseIsOver);
            else bossEvents.Push(BossEvent.PhaseIsNotOver);
        }

        public void StartLaserAttack()
        {
            currentPhaseCoroutine = StartCoroutine(LaserAttack());
        }

        private IEnumerator LaserAttack()
        {
            phaseIsOver = false;
            // Avertit le joueur de l'attaque de laser
            foreach (var laser in lasers)
            {
                laser.gameObject.SetActive(true);
                laser.BoxCollider2D.enabled = false;

                ChangeLaserOpacity(laser, warningOpacity);
            }
            yield return new WaitForSeconds(alertDelay);
            
            // Termine l'avertissement
            foreach (var laser in lasers)
            {
                ChangeLaserOpacity(laser, FULL_OPACITY);
                laser.BoxCollider2D.enabled = true;
            }

            // Commennce l'attaque de laser
            StartCoroutine(LaserAttackEffect());
        }

        private void ChangeLaserOpacity(Laser laser, float newOpacity)
        {
            var color = laser.SpriteRenderer.color;
            laser.SpriteRenderer.color = new Color(color.r, color.g, color.b, newOpacity);
        }
        
        private IEnumerator LaserAttackEffect()
        {
            float opacity = FULL_OPACITY;
            while (opacity > 0)
            {
                yield return new WaitForSeconds(laserEffectDelay);
                opacity -= laserOpacityEffect;
                foreach (var laser in lasers)
                {
                    ChangeLaserOpacity(laser, opacity);
                }
            }

            phaseIsOver = true;
        }

        public void DeactivateLasers()
        {
            foreach (var laser in lasers)
            {
                laser.gameObject.SetActive(false);
            }
        }
        
        public void StartTurretAttack()
        {
            currentPhaseCoroutine = StartCoroutine(TurretAttack());
        }

        private IEnumerator TurretAttack()
        {
            phaseIsOver = false;
            ChangeEnabledTurrets(false);
            ChangeActiveTurrets(true);
            
            // Avertit le joueur de l'attaque de tourelles
            foreach (var turret in turrets)
            {
                turret.TargetAimRotation = Quaternion.Euler(turret.TargetAimRotation.x, turret.TargetAimRotation.y,
                    zTurretIdleAim);
                turret.CurrentAimRotation = Quaternion.Euler(turret.CurrentAimRotation.x, turret.CurrentAimRotation.y,
                    zTurretIdleAim);
            }
            
            yield return new WaitForSeconds(alertDelay);

            // Termine l'avertissement
            foreach (var turret in turrets)
            {
                turret.TargetAimRotation = turret.StandbyAimRotation;
            }

            // Commence l'attaque
            ChangeEnabledTurrets(true);

            yield return new WaitUntil(IsTurretAttackOver);

            phaseIsOver = true;
        }

        /// <summary>
        /// Vérifie que l'attaque des tourelles est finit ou non
        /// </summary>
        /// <returns>true si l'attaque est finit, false sinon</returns>
        private bool IsTurretAttackOver()
        {
            return turrets.All(it => !it.enabled);
        }

        public void ChangeActiveTurrets(bool newActive)
        {
            foreach (var turret in turrets)
            {
                turret.gameObject.SetActive(newActive);
            }
        }

        private void ChangeEnabledTurrets(bool newEnabled)
        {
            foreach (var turret in turrets)
            {
                turret.enabled = newEnabled;
            }
        }

        public void StartExplosionAttack()
        {
            currentPhaseCoroutine = StartCoroutine(ExplosionAttack());
        }

        private IEnumerator ExplosionAttack()
        {
            phaseIsOver = false;

            // Avertit le joueur que l'attaque d'explosion va se produire
            foreach (var bossSpriteRenderer in bossSpriteRenderers)
            {
                bossSpriteRenderer.color = alertAcolor;
            }

            yield return new WaitForSeconds(alertDelay);

            // Termine l'avertissement
            foreach (var bossBodySpriteRenderer in bossSpriteRenderers)
            {
                bossBodySpriteRenderer.color = origColor;
            }
            
            // Lance l'attaque d'explosion
            ChangeActiveExplosion(true);
            var origScale = explosion.Scale;
            
            // Augmente la grosseur de l'explosion
            while (explosion.Scale.x < maximumExplosionScale)
            {
                explosion.Scale = new Vector3(explosion.Scale.x + explosionScaleChange,
                    explosion.Scale.y + explosionScaleChange, explosion.Scale.z);
                yield return new WaitForSeconds(explosionScaleChangeDelay);
            }

            explosion.Scale = origScale;
            phaseIsOver = true;
        }

        public void ChangeActiveExplosion(bool newActive)
        {
            explosion.gameObject.SetActive(newActive);
        }

        public void StartStunPhase()
        {
            currentPhaseCoroutine = StartCoroutine(StunPhase());
        }

        private IEnumerator StunPhase()
        {
            phaseIsOver = false;
            
            // Active les boutons pour ouvrir les portes
            ChangeActiveStun(true);

            yield return new WaitForSeconds(stunTime);
            
            phaseIsOver = true;
            // Réinitialise les portes
            ResetStun();
        }

        private void ResetStun()
        {
            foreach (var buttonTrigger in buttonTriggers)
            {
                buttonTrigger.Reset();
            }
        }

        public void ChangeActiveStun(bool newActive)
        {
            foreach (var buttonTrigger in buttonTriggers)
            {
                buttonTrigger.gameObject.SetActive(newActive);
            }

            foreach (var prisonner in prisonners)
            {
                prisonner.gameObject.SetActive(newActive);
            }
        }

        [ContextMenu("Deactivate all")]
        private void DeactivateAllPhases()
        {
            ChangeActiveTurrets(false);
            DeactivateLasers();
            ChangeActiveExplosion(false);
            ChangeActiveStun(false);
        }
        
        public void ResetToOriginalState()
        {
            StopCoroutine(currentPhaseCoroutine);
            DeactivateAllPhases();
            bossEvents = new Stack<BossEvent>();
            phaseIsOver = false;
            stateMachine.Reset();
        }

#if UNITY_EDITOR
        [ContextMenu("Disable turrets")]
        private void DisableTurret()
        {
            foreach (var turret in turrets)
            {
                turret.Kill();   
            }
        }
#endif
    }
}