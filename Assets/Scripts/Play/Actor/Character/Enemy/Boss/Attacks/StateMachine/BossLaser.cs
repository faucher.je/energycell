﻿namespace Game
{
    //Author: Jérémy Faucher
    public class BossLaser : IState
    {
        private BossPhaseController bossPhaseController;

        public BossLaser(BossPhaseController bossPhaseController)
        {
            this.bossPhaseController = bossPhaseController;
        }

        public void Enter()
        {
            bossPhaseController.StartLaserAttack();
        }

        public IState Update()
        {
            var bossEvent = bossPhaseController.Read();
            switch (bossEvent)
            {
                case BossEvent.PhaseIsOver:
                    return new BossExplosion(bossPhaseController);
                default:
                    return this;
            }
        }

        public void Leave()
        {
            bossPhaseController.DeactivateLasers();
        }
    }
}