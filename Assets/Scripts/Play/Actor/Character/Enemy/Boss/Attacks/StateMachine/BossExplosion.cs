﻿namespace Game
{
    //Author: Jérémy Faucher
    public class BossExplosion: IState
    {
        private BossPhaseController bossPhaseController;

        public BossExplosion(BossPhaseController bossPhaseController)
        {
            this.bossPhaseController = bossPhaseController;
        }

        public void Enter()
        {
            bossPhaseController.StartExplosionAttack();
        }

        public IState Update()
        {
            var bossEvent = bossPhaseController.Read();
            switch (bossEvent)
            {
                case BossEvent.PhaseIsOver:
                    return new BossStun(bossPhaseController);
                default:
                    return this;
            }
        }

        public void Leave()
        {
            bossPhaseController.ChangeActiveExplosion(false);
        }
    }
}