﻿namespace Game
{
    public class BossStun : IState
    {
        private BossPhaseController bossPhaseController;

        public BossStun(BossPhaseController bossPhaseController)
        {
            this.bossPhaseController = bossPhaseController;
        }

        public void Enter()
        {
            bossPhaseController.StartStunPhase();
        }

        public IState Update()
        {
            var bossEvent = bossPhaseController.Read();
            switch (bossEvent)
            {
                case BossEvent.PhaseIsOver:
                    return new BossTurrets(bossPhaseController);
                default:
                    return this;
            }
        }

        public void Leave()
        {
            bossPhaseController.ChangeActiveStun(false);
        }
    }
}