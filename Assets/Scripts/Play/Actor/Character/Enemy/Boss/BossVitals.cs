﻿using Harmony;
using UnityEngine;

namespace Game
{
    //Author: Jérémy Faucher
    public class BossVitals : MonoBehaviour
    {
        [SerializeField] private float maxBossHealth = 100f;

        private BossHitEventChannel bossHitEventChannel;

        public float MaxBossHealth => maxBossHealth;
        
        public float Health { get; private set; }
        
        protected void Awake()
        {
            Health = maxBossHealth;
            bossHitEventChannel = Finder.BossHitEventChannel;
        }

        public void Hit(float damage)
        {
            Health -= damage;
            if (damage > 0) bossHitEventChannel.Publish(Health);
        }

        public void ResetToOriginalState()
        {
            Health = MaxBossHealth;
            bossHitEventChannel.Publish(Health);
        }
    }
}