﻿using System;
using System.Collections;
using Harmony;
using UnityEngine;

//Author: Rebecca Brassard
namespace Game
{
    public class Berserker : MovableEnemy
    {
        [SerializeField] private float rushCooldown = 4f;
        private Animator animator;
        private bool isRushing = false;
        private bool canRush = true;
        private Vector3 rushTarget;
        private bool doAttackAnimation = true;
        private Collider2D collider;

        protected override void Awake()
        {
            base.Awake();
            animator = GetComponentInChildren<Animator>();
            animator.SetBool(AnimatorParameters.Walk, true);
            collider = GetComponent<Collider2D>();
        }

        protected override void FixedUpdate()
        {
            if (isRushing)
            {
                Rush();
            }
            else if (TargetSpotted && canRush && IsFacingPlayer())
            {
                StartRushingPlayer();
            }
            else
            {
                base.FixedUpdate();
            }
        }

        private bool IsFacingPlayer()
        {
            if (Math.Abs(transform.position.y - LastKnownTargetPosition.y) <= 1)
            {
                if (LastKnownTargetPosition.x > transform.position.x && IsLookingRight)
                    return true;
                else if (LastKnownTargetPosition.x < transform.position.x && !IsLookingRight)
                    return true;
            }

            return false;
        }

        private void StartRushingPlayer()
        {
            animator.SetBool(AnimatorParameters.Run, true);
            animator.SetBool(AnimatorParameters.Walk, false);
            rushTarget = LastKnownTargetPosition;
            isRushing = true;
            canRush = false;
        }

        private void Rush()
        {
            if (!IsGrounded())
            {
                EndRush();
            }
            else if (Math.Abs(rushTarget.x - transform.position.x) <= 0.3f)
            {
                if (TargetSpotted && IsFacingPlayer())
                {
                    StartRushingPlayer();
                    return;
                }
                EndRush();
            }
            else if (rushTarget.x > transform.position.x)
            {
                MoveRight(WalkSpeed * 2);
            }
            else
            {
                MoveLeft(WalkSpeed * 2);
            }
        }

        private void EndRush()
        {
            rigidbody2D.velocity = new Vector2(0, rigidbody2D.velocity.y);
            isRushing = false;
            animator.SetBool(AnimatorParameters.Run, false);
            animator.SetBool(AnimatorParameters.Walk, true);
            IEnumerator coroutine = ReEnableRushAbility();
            StartCoroutine(coroutine);
        }

        private IEnumerator ReEnableRushAbility()
        {
            yield return new WaitForSeconds(rushCooldown);
            canRush = true;
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.collider.CompareTag(Tags.Player))
            {
                animator.SetTrigger(AnimatorParameters.Slash);
                Target.Knockback(transform.position);
                Target.Hurt(DamageDealtToPlayerOnHit);
            }
        }
        private bool IsGrounded()
        {
            RaycastHit2D[] hit;
            ContactFilter2D filter2D = new ContactFilter2D
            {
                useTriggers = false,
                useLayerMask = false
            };
            hit = new RaycastHit2D[3];
            collider.Raycast(Vector2.down,filter2D, hit, 0.5f);
            return hit[0].collider != null;
        }
    }
}