﻿using System.Collections;
using Harmony;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Game
{
    // Author: Mathieu Boutet
    public class Turret : SyntheticEnemy
    {
        [Header("Rotation")]
        [SerializeField] [Tooltip("The rotation speed in degrees per second")]
        private float rotationSpeed = 270;

        [Header("Attack")]
        [SerializeField]
        private float burstDuration = 2;

        [SerializeField]
        private float fireDelay = 0.15f;

        [SerializeField]
        private float maxShootingAngleDifference = 4;

        [Header("Projectiles")]
        [SerializeField]
        private GameObject projectilePrefab = null;

        [SerializeField]
        private float projectileMaxLifetime = 10;

        [SerializeField]
        private int projectilePoolSize = 100;

        [SerializeField]
        private float projectileSpeed = 7.5f;

        [SerializeField]
        private string projectilesContainerObjectName = "Projectiles";

        // Les rotations sont locales (relatives au parent)
        private Quaternion standbyAimRotation;

        private ObjectPool<TurretProjectile> projectilePool;
        private bool isShooting;

        public Quaternion StandbyAimRotation => standbyAimRotation;
        public Quaternion TargetAimRotation { get; set; }

        public bool TargetAimRotationReached => CurrentAimRotation == TargetAimRotation;
        public bool LockedOnTarget => isShooting;
        public float ProjectileMaxLifetime => projectileMaxLifetime;
        
        public Quaternion CurrentAimRotation
        {
            get => HeadTransform.localRotation;
            set => HeadTransform.localRotation = value;
        }

        public override bool TargetSpotted
        {
            protected set
            {
                base.TargetSpotted = value;
                if (value)
                    FindAimRotationToTarget();
                else
                    TargetAimRotation = standbyAimRotation;
            }
        }

        protected override void Awake()
        {
            base.Awake();
            
            InitializeProjectilePool();
            standbyAimRotation = CurrentAimRotation;
            TargetAimRotation = standbyAimRotation;
            isShooting = false;
        }

        protected void Update()
        {
            if (LockedOnTarget) return;

            SearchForTarget();

            if (TargetAimRotationReached)
            {
                if (TargetSpotted)
                    StartCoroutine(ShootBurst());
            }
            else
            {
                RotateTowardsTargetDirection();
            }
        }

        private void FindAimRotationToTarget()
        {
            Vector2 position = HeadTransform.position;
            var directionToTarget = position.DirectionTo(LastKnownTargetPosition).normalized;
            // Quaternion.LookRotation prend les axes z (forward) et y (up) de la rotation en paramètre.
            // On veut que l'axe x de la rotation pointe vers la cible.
            var forward = HeadTransform.forward;
            var worldRotation = Quaternion.LookRotation(
                // L'axe z ne change pas en 2D
                forward,
                // On peut trouver l'axe y à partir des 2 angles qu'on connait déjà
                // L'ordre des paramètres est déterminé selon la règle de la main gauche:
                // https://docs.unity3d.com/ScriptReference/Vector3.Cross.html
                Vector3.Cross(forward, directionToTarget)
            );
            TargetAimRotation = HeadTransform.WorldToLocalRotation(worldRotation);
        }

        private void RotateTowardsTargetDirection()
        {
            var newAngle = Mathf.MoveTowardsAngle(
                CurrentAimRotation.eulerAngles.z,
                TargetAimRotation.eulerAngles.z,
                rotationSpeed * Time.deltaTime
            );

            if (newAngle == TargetAimRotation.eulerAngles.z)
                CurrentAimRotation = TargetAimRotation;
            else
                CurrentAimRotation = Quaternion.AngleAxis(newAngle, Vector3.forward);
        }

        private IEnumerator ShootBurst()
        {
            isShooting = true;
            var shootCoroutine = ShootContinuously();
            StartCoroutine(shootCoroutine);
            yield return new WaitForSeconds(burstDuration);
            StopCoroutine(shootCoroutine);
            isShooting = false;
        }

        private IEnumerator ShootContinuously()
        {
            while (isActiveAndEnabled)
            {
                yield return new WaitForSeconds(fireDelay);
                FireBullet();
            }
        }

        private void FireBullet()
        {
            var projectile = projectilePool.GetObject();
            projectile.gameObject.SetActive(true);
            
            var projectileTransform = projectile.transform;

            projectileTransform.position = HeadTransform.position;
            projectileTransform.rotation = HeadTransform.rotation;
            projectileTransform.Rotate(0, 0, Random.Range(-maxShootingAngleDifference, maxShootingAngleDifference));

            projectile.Rigidbody2D.velocity = projectileTransform.right * projectileSpeed;
        }

        private void InitializeProjectilePool()
        {
            var projectilesContainer = new GameObject(projectilesContainerObjectName);
            projectilesContainer.transform.parent = transform;
            projectilePrefab.SetActive(false);

            projectilePool = new ObjectPool<TurretProjectile>(
                () => CreateProjectile(projectilesContainer),
                projectilePoolSize
            );
        }

        private TurretProjectile CreateProjectile(GameObject containerObject)
        {
            var projectile = Instantiate(
                projectilePrefab,
                HeadTransform.position,
                HeadTransform.rotation,
                containerObject.transform
            );
            var script = projectile.GetComponent<TurretProjectile>();
            script.Turret = this;
            return script;
        }

        public void RemoveProjectile(TurretProjectile projectile)
        {
            projectile.gameObject.SetActive(false);
            projectilePool.PutObject(projectile);
        }

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            // Shooting cone
            var transform1 = HeadTransform;
            var position = transform1.position;
            var right = transform1.right;
            var positiveRotation = Quaternion.AngleAxis(maxShootingAngleDifference, Vector3.forward);
            var negativeRotation = Quaternion.AngleAxis(-maxShootingAngleDifference, Vector3.forward);
            
            var originalColor = Gizmos.color;
            Gizmos.color = Color.red;
            Gizmos.DrawRay(position, positiveRotation * right);
            Gizmos.DrawRay(position, negativeRotation * right);
            Gizmos.color = originalColor;
        }
#endif
    }
}