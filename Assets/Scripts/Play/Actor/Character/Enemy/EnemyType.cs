﻿namespace Game
{
    public enum EnemyType
    {
        FleshEnemy,
        SyntheticEnemy,
        Boss
    }
}