﻿namespace Game
{
    public abstract class SyntheticEnemy : Enemy
    {
        // Author : Samuel Ménard
        protected override void Awake()
        {
            base.Awake();
            EnemyType = EnemyType.SyntheticEnemy;
        }

        // Author : Mathieu Boutet
        public override void Tase()
        {
            Kill();
        }
        
        // Author : Mathieu Boutet
        public override void Kill()
        {
            enabled = false;
            PlayDeathAnimation();
            EnemyDeathEventChannel.Publish(EnemyType);
        }

        private void PlayDeathAnimation()
        {
            //TODO: Jouer l'animation que l'ennemi synthétique meurt
        }
    }
}