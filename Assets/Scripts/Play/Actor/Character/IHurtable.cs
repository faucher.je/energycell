﻿namespace Game
{
    // Author : Samuel Ménard
    public interface IHurtable
    {
        void Hurt(float damage);
    }
}