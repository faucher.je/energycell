﻿namespace Game
{
    // Author : Rebecca Brassard
    public interface IKillable
    {
        void Kill();
    }
}