﻿namespace Game
{
    // Author: Jérémy Faucher
    public interface ICharacter: IEntity, IKillable, IHurtable, IKnockable
    {
        
    }
}