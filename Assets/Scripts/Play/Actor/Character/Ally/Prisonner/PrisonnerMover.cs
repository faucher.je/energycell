﻿using System;
using Harmony;
using UnityEngine;

namespace Game
{
    //Author: Jérémy Faucher
    [RequireComponent(typeof(Rigidbody2D))]
    public class PrisonnerMover : MonoBehaviour
    {
        [SerializeField] private float speed = 5f;
        
        private const int Y_FLIP_ROTATION = 180;
        private Vector2 direction = Vector2.right;
        private Boss boss;
        private Animator prisonerAnimator;

        private Rigidbody2D rigidbody2D;
        private void Awake()
        {
            prisonerAnimator = GetComponentInChildren<Animator>();
            rigidbody2D = GetComponent<Rigidbody2D>();
            boss = Finder.Boss;
        }

        private void OnEnable()
        {
            if (boss.transform.position.x < transform.position.x)
            {
                direction = Vector2.left;
                transform.rotation = Quaternion.Euler(0, Y_FLIP_ROTATION, 0);
            }
            else
            {
                direction = Vector2.right;
            }
            rigidbody2D.velocity = direction * speed;
            prisonerAnimator.SetBool(AnimatorParameters.Run, true);
        }

        private void Update()
        {
            var wantedSpeed = direction * speed;
            if (rigidbody2D.velocity.x < wantedSpeed.x * direction.x)
            {
                rigidbody2D.velocity = wantedSpeed;
            }
        }
    }
}