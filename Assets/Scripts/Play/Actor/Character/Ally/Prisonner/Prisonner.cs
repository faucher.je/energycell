﻿using Harmony;
using UnityEngine;

namespace Game
{
    //Author: Jérémy Faucher
    [RequireComponent(typeof(BoxCollider2D))]
    public class Prisonner : MonoBehaviour
    {
        [SerializeField] private float damage = 20f;
        
        private Boss boss;
        private Vector3 origPosition;

        private void Awake()
        {
            origPosition = transform.position;
        }

        private void OnEnable()
        {
            boss = Finder.Boss;
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.CompareTag(Tags.Boss))
            {
                boss.BossVitals.Hit(damage);
                transform.position = origPosition;
                gameObject.SetActive(false);
            }
        }
    }
}