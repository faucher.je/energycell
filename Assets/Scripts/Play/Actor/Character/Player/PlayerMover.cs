using System;
using System.Collections;
using Harmony;
using UnityEditor;
using UnityEngine;

namespace Game
{
    public class PlayerMover : MonoBehaviour
    {
        [SerializeField, Tooltip("Player speed in units per second")]
        private float walkSpeed = 3.5f;

        [SerializeField] private float jumpForce = 4.75f;
        [SerializeField] private float jumpMaxDuration = 0.2f;
        [SerializeField] private float maxVelocityMagnitude = 10;
        [SerializeField] private float forceXAxisOnEnemyHit = 500;
        [SerializeField] private float forceYAxisOnEnemyHit = 500;
        [SerializeField] private float rotationalOffset = 180f;
        [SerializeField] private float timeToFlip = 0.5f;
        [SerializeField] private float timeCantMoveAfterHit = 0.75f;
        [SerializeField] private bool hasDoubleJumpGadget;
        [SerializeField] private float minimalGroundDistance = 0.5f;
        [SerializeField] private ContactFilter2D groundContactFilter;

        private Animator playerAnimator;
        private DimensionChangeEventChannel dimensionChangeEventChannel;
        private DimensionChangeController dimensionChangeController;
        private new Rigidbody2D rigidbody2D;
        private float currentWalkVelocity;
        private new BoxCollider2D collider;
        private bool isJumping;
        private bool canMove;
        private bool jumpTriggered;
        private bool hasUsedDoubleJump;
        private float jumpStartTime;

        // Author : Mathieu Boutet
        // https://forum.unity.com/threads/checking-for-grounded-state-with-boxcast-platforms.879412/#post-5782534
        private bool IsGrounded => rigidbody2D.IsTouching(groundContactFilter);

        // Author : Mickael Chabot
        public bool HasDoubleJumpGadget
        {
            get => hasDoubleJumpGadget;
            set => hasDoubleJumpGadget = value;
        }

        // Author : Mathieu Boutet
        private bool ShouldWalk
        {
            get
            {
                if (currentWalkVelocity != 0)
                {
                    var xVelocity = rigidbody2D.velocity.x;
                    if (Math.Abs(xVelocity) < Math.Abs(currentWalkVelocity)
                        || Math.Sign(xVelocity) != Math.Sign(currentWalkVelocity))
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        private void Awake()
        {
            playerAnimator = GetComponentInChildren<Animator>();
            playerAnimator.SetBool(AnimatorParameters.Stand, true);
            rigidbody2D = GetComponent<Rigidbody2D>();
            collider = GetComponent<BoxCollider2D>();
            dimensionChangeEventChannel = Finder.DimensionChangeEventChannel;
            dimensionChangeController = Finder.DimensionChangeController;
            isJumping = false;
            canMove = true;
            hasUsedDoubleJump = false;
            jumpTriggered = false;
            jumpStartTime = 0;

            dimensionChangeEventChannel.OnDimensionChange += OnDimensionChange;
        }

        // Author : Mathieu Boutet
        private void FixedUpdate()
        {
            if (canMove)
            {
                if (ShouldWalk)
                    ApplyWalkVelocity();

                if (isJumping)
                {
                    if (Time.time - jumpStartTime < jumpMaxDuration)
                        ApplyJumpVelocity();
                    else
                        StopJumping();
                }
            }

            ClampVelocity();
        }

        private void Update()
        {
            // Permet de sauter quand même si le bouton de jump est appuyé juste avant
            // de toucher le sol mais qu'il est encore enfoncé en arrivant au sol
            if (jumpTriggered && !isJumping && IsGrounded)
                StartJumping();
        }

        private void OnDestroy()
        {
            dimensionChangeEventChannel.OnDimensionChange -= OnDimensionChange;
        }

        // Author : Mathieu Boutet
        private void ApplyWalkVelocity()
        {
            rigidbody2D.velocity = new Vector2(currentWalkVelocity, rigidbody2D.velocity.y);
        }

        // Author : Mathieu Boutet
        private void ApplyJumpVelocity()
        {
            rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, jumpForce);
        }

        // Author : Mathieu Boutet
        private void ClampVelocity()
        {
            rigidbody2D.velocity = Vector2.ClampMagnitude(rigidbody2D.velocity, maxVelocityMagnitude);
        }

        // Author : Mathieu Boutet
        public void StartWalking(float value)
        {
            playerAnimator.SetBool(AnimatorParameters.Stand, false);
            playerAnimator.SetBool(AnimatorParameters.Run, true);
            currentWalkVelocity = value * walkSpeed;
            rigidbody2D.velocity = new Vector2(currentWalkVelocity, rigidbody2D.velocity.y);
        }

        // Author : Mathieu Boutet
        public void StopWalking()
        {
            playerAnimator.SetBool(AnimatorParameters.Run, false);
            playerAnimator.SetBool(AnimatorParameters.Stand, true);
            rigidbody2D.velocity = new Vector2(0, rigidbody2D.velocity.y);
            currentWalkVelocity = 0;
        }

        // Author : Mathieu Boutet & Mickael Chabot
        public void StartJumping()
        {
            jumpTriggered = true;
            
            if (IsGrounded)
            {
                hasUsedDoubleJump = false;
                Jump();
            }
            else if (HasDoubleJumpGadget && !hasUsedDoubleJump)
            {
                hasUsedDoubleJump = true;
                Jump();
            }
        }

        // Author : Mathieu Boutet
        public void StopJumping()
        {
            jumpTriggered = false;
            isJumping = false;

            playerAnimator.SetBool(AnimatorParameters.Jump, false);
        }

        // Author : Mathieu Boutet
        private void Jump()
        {
            jumpStartTime = Time.time;
            isJumping = true;
            ApplyJumpVelocity();
            playerAnimator.SetBool(AnimatorParameters.Jump, true);
        }

        //Author: Dave Ouellet
        private void OnDimensionChange(Dimension newDimension)
        {
            Vector2 direction;
            if (newDimension == Dimension.First)
                direction = Vector2.down;
            else
                direction = Vector2.up;

            transform.Translate(direction * dimensionChangeController.DistanceBetweenDimensions);
        }

        // Author: Rebecca
        public void GoDownPlatform()
        {
            RaycastHit2D[] hit;
            if (IsOnPlatform(out hit))
            {
                if (hit[0].collider.CompareTag(Tags.Platform))
                {
                    var platformEffector = hit[0].collider.gameObject.GetComponent<PlatformEffector2D>();
                    StartCoroutine(FlipEffector(platformEffector));
                }
            }
        }

        // Author: Jérémy Faucher
        private IEnumerator FlipEffector(PlatformEffector2D platformEffector)
        {
            platformEffector.rotationalOffset += rotationalOffset;
            yield return new WaitForSeconds(timeToFlip);
            platformEffector.rotationalOffset -= rotationalOffset;
        }

        // Author: Rebecca
        private bool IsOnPlatform(out RaycastHit2D[] hit)
        {
            ContactFilter2D filter2D = new ContactFilter2D
            {
                useTriggers = false,
                useLayerMask = false
            };
            hit = new RaycastHit2D[3];
            collider.Raycast(Vector2.down, filter2D, hit, minimalGroundDistance);
            return hit[0].collider != null;
        }

        // Author: Rebecca
        public void ReceiveKnockback(Vector2 otherPosition)
        {
            canMove = false;
            IEnumerator coroutine = ReEnablePlayerMovement();
            StartCoroutine(coroutine);
            if (transform.position.x > otherPosition.x)
            {
                rigidbody2D.AddForce(new Vector2(forceXAxisOnEnemyHit, forceYAxisOnEnemyHit));
            }
            else
            {
                rigidbody2D.AddForce(new Vector2(-forceXAxisOnEnemyHit, forceYAxisOnEnemyHit));
            }
        }

        // Author: Rebecca
        private IEnumerator ReEnablePlayerMovement()
        {
            yield return new WaitForSeconds(timeCantMoveAfterHit);
            canMove = true;
        }

        // Author : Rebecca
        public void ResetVelocity()
        {
            /*
             *  Mettez moi pas de commentaire pour dire que j'aurai pu faire sa comme sa:
             *  rigidbody2D.angularVelocity = 0.0f;
             *  rigidbody2D.velocity = Vector2.zero;
             *  Sa marche pas
             */
            rigidbody2D.isKinematic = true;
            rigidbody2D.isKinematic = false;
        }

#if UNITY_EDITOR
        // Author : Mathieu Boutet
        private void OnDrawGizmos()
        {
            // Velocity
            if (EditorApplication.isPlaying)
            {
                var position = transform.position;
                GizmosExtensions.DrawLine(position, position + (Vector3) rigidbody2D.velocity, Color.yellow);
            }
        }
#endif
    }
}