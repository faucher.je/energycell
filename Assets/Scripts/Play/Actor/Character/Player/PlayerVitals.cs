﻿using System;
using System.Collections;
using Harmony;
using UnityEngine;

namespace Game
{
    public class PlayerVitals : MonoBehaviour
    {
        [SerializeField] private float minHealthPoints = 0;
        [SerializeField] private float maxHealthPoints = 100;
        [SerializeField] private float minHeatPoints = 0;
        [SerializeField] private float maxHeatPoints = 100;
        [SerializeField] private float hpLostPerSecondWhenHeatIsFull = 1;
        [SerializeField] private float timeInvulnerableToEnemyAfterHit = 3.0f;
        [SerializeField] private float heatGainedOrLostPerSecond = 5f;

        private PlayerHealthChangeEventChannel playerHealthChangeEventChannel;
        private PlayerHeatChangeEventChannel playerHeatChangeEventChannel;
        private Player player;
        private float currentHealth;
        private float currentHeat;

        private Coroutine heatUpCoroutine;
        private Coroutine coolDownCoroutine;

        public bool HasPowerUpEnergy { get; set; }

        public bool IsInvincible { get; set; }

        public float CurrentHealth
        {
            get => currentHealth;
            private set
            {
                if (value == currentHealth) return;
                
                currentHealth = value;
                playerHealthChangeEventChannel.Publish(CurrentHealth);
            }
        }

        public float CurrentHeat
        {
            get => currentHeat;
            private set
            {
                if (value == currentHeat) return;

                currentHeat = value;
                playerHeatChangeEventChannel.Publish(CurrentHeat);
            }
        }

        public float MaxHealthPoints => maxHealthPoints;

        public float MaxHeatPoints => maxHeatPoints;

        public float MinHealthPoints => minHealthPoints;

        private void Awake()
        {
            IsInvincible = false;
            playerHealthChangeEventChannel = Finder.PlayerHealthChangeEventChannel;
            playerHeatChangeEventChannel = Finder.PlayerHeatChangeEventChannel;
            player = Finder.Player;
            currentHealth = 0;
            currentHeat = 0;
        }

        private void Start()
        {
            ResetValues();
        }
        
        // Author: Rebecca
        private void Update()
        {
            if (CurrentHeat >= MaxHeatPoints)
            {
                ReceiveDamage(hpLostPerSecondWhenHeatIsFull * Time.deltaTime);
            }
        }
        
        // Author: Rebecca
        public void ReceiveDamage(float damage)
        {
            CurrentHealth -= damage;
            if (CurrentHealth <= minHealthPoints)
            {
                player.Kill();
            }
        }
        
        private IEnumerator RemoveInvincibility()
        {
            yield return new WaitForSeconds(timeInvulnerableToEnemyAfterHit);
            IsInvincible = false;
        }

        //Author: Dave Ouellet
        public void HeatUp(float heatAmount)
        {
            CurrentHeat = Math.Min(CurrentHeat + heatAmount, MaxHeatPoints);
        }

        //Author: Samuel Ménard
        public void ActivateHeatUpRoutine()
        {
            StopCoolDownRoutine();
            if(heatUpCoroutine == null)
                heatUpCoroutine = StartCoroutine(HeatUpRoutine());
        }
        
        //Author: Dave Ouellet
        private IEnumerator HeatUpRoutine()
        {
            while (CurrentHeat < MaxHeatPoints)
            {
                HeatUp(heatGainedOrLostPerSecond * Time.deltaTime);
                yield return null;
            }

            //To show that the coroutine is over.
            heatUpCoroutine = null;
        }
        
        //Author: Samuel Ménard
        public void StopHeatUpRoutine()
        {
            if (heatUpCoroutine != null)
            {
                StopCoroutine(heatUpCoroutine);
                //To show that the coroutine is over.
                heatUpCoroutine = null;
            }
        }

        //Author: Dave Ouellet
        public void CoolDown(float heatAmount)
        {
            CurrentHeat = Math.Max(CurrentHeat - heatAmount, minHeatPoints);
        }

        //Author: Samuel Ménard
        public void ActivateCoolDownRoutine()
        {
            StopHeatUpRoutine();
            if(coolDownCoroutine == null)
                coolDownCoroutine = StartCoroutine(CoolDownRoutine());
        }
        
        //Author: Dave Ouellet
        private IEnumerator CoolDownRoutine()
        {
            while (CurrentHeat > minHeatPoints)
            {
                CoolDown( heatGainedOrLostPerSecond * Time.deltaTime);
                yield return null;
            }
            
            //To show that the coroutine is over.
            coolDownCoroutine = null;
        }
        
        //Author: Samuel Ménard
        public void StopCoolDownRoutine()
        {
            if (coolDownCoroutine != null)
            {
                StopCoroutine(coolDownCoroutine);
                //To show that the coroutine is over.
                coolDownCoroutine = null;
            }

        }
        
        // Author : Mickael Chabot & Rebecca Brassard
        public void Heal(float healing)
        {
            CurrentHealth = Math.Min(CurrentHealth + healing, MaxHealthPoints);
        }
        
        public void ResetValues()
        {
            CurrentHealth = MaxHealthPoints;
            CurrentHeat = minHeatPoints;
        }
        
        // Author : Mickael Chabot
        public IEnumerator InfiniteEnergyRoutine(float secondsOfInfiniteEnergy)
        {
            yield return new WaitForSeconds(secondsOfInfiniteEnergy);
            player.OnStopPowerUpEnergy();
        }
        
        public void GainInvincibility()
        {
            IsInvincible = true;
            StartCoroutine(RemoveInvincibility());
        }
    }
}