using Harmony;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Game
{
    [Findable(Tags.Player)]
    [RequireComponent(typeof(PlayerMover))]
    [RequireComponent(typeof(PlayerVitals))]
    [RequireComponent(typeof(BoxCollider2D))]
    public class Player : MonoBehaviour, ICharacter
    {
        [SerializeField] private CheckPoint defaultRespawn;
        [SerializeField] private bool hasTaserGadget = false;
        [SerializeField] private bool hasDimensionGadget = false;
         
        private DimensionChangeEventChannel dimensionChangeEventChannel;
        private DimensionChangeController dimensionChangeController;
        private InputActions.GameActions inputs;

        public PlayerMover Mover { get; private set; }
        public PlayerVitals Vitals { get; private set; }
        public BoxCollider2D Collider2D { get; private set; }

        // Author : Samuel Ménard
        public CheckPoint CurrentCheckPoint { get; set; }

        // Author : Mickael Chabot
        public bool HasTaserGadget
        {
            get => hasTaserGadget;
            set => hasTaserGadget = value;
        }

        // Author : Mickael Chabot
        public bool HasDimensionGadget
        {
            get => hasDimensionGadget;
            set => hasDimensionGadget = value;
        }

        private void Awake()
        {
            dimensionChangeEventChannel = Finder.DimensionChangeEventChannel;
            dimensionChangeController = Finder.DimensionChangeController;
            inputs = Finder.Inputs.Actions.Game;
            Mover = this.GetRequiredComponent<PlayerMover>();
            Vitals = this.GetRequiredComponent<PlayerVitals>();
            Collider2D = this.GetRequiredComponent<BoxCollider2D>();
            CurrentCheckPoint = defaultRespawn;
        }
        
        private void Start()
        {
            if (CurrentCheckPoint != null) transform.position = CurrentCheckPoint.SpawnPosition;
        }
        
        private void OnEnable()
        {
            RegisterEvents();
        }
        
        private void OnDisable()
        {
            UnregisterEvents();
        }
        
        // Author : Mathieu Boutet
        private void OnMovePerformed(InputAction.CallbackContext context) =>
            Mover.StartWalking(context.ReadValue<float>());
        private void OnMoveCanceled(InputAction.CallbackContext context) => Mover.StopWalking();
        private void OnJumpStarted(InputAction.CallbackContext context) => Mover.StartJumping();
        private void OnJumpCanceled(InputAction.CallbackContext context) => Mover.StopJumping();
        private void OnGoDownPlatform(InputAction.CallbackContext context) => Mover.GoDownPlatform();

        // Author : Mathieu Boutet
        private void RegisterEvents()
        {
            inputs.Move.performed += OnMovePerformed;
            inputs.Move.canceled += OnMoveCanceled;
            inputs.Jump.started += OnJumpStarted;
            inputs.Jump.canceled += OnJumpCanceled;
            inputs.GoDownPlatform.performed += OnGoDownPlatform;
            dimensionChangeEventChannel.OnDimensionChange += OnDimensionChange;
        }

        // Author : Mathieu Boutet
        private void UnregisterEvents()
        {
            inputs.Move.performed -= OnMovePerformed;
            inputs.Move.canceled -= OnMoveCanceled;
            inputs.Jump.started -= OnJumpStarted;
            inputs.Jump.canceled -= OnJumpCanceled;
            inputs.GoDownPlatform.performed -= OnGoDownPlatform;
            dimensionChangeEventChannel.OnDimensionChange -= OnDimensionChange;
        }

        private void Respawn()
        {
            Vitals.ResetValues();
            Mover.ResetVelocity();

            dimensionChangeController.ResetDimensionToFirstDimension();

            if (CurrentCheckPoint != null)
            {
                CurrentCheckPoint.ResetElements();
                transform.position = CurrentCheckPoint.SpawnPosition;
            }
        }

        // Author : Mickael Chabot
        public void Hurt(float damageReceived)
        {
            if(!Vitals.IsInvincible)
            {
                Vitals.GainInvincibility();
                Vitals.ReceiveDamage(damageReceived);
            }
        }

        // Author : Mickael Chabot
        public void HeatUp(float heatAmount)
        {
            if (Vitals.HasPowerUpEnergy) return;
            
            Vitals.HeatUp(heatAmount);
            if(dimensionChangeController.CurrentDimension == Dimension.First)
                Vitals.ActivateCoolDownRoutine();
        }

        [ContextMenu("Kill")]
        public void Kill()
        {
            Respawn();
        }


        //Author: Dave Ouellet
        private void OnDimensionChange(Dimension currentDimension)
        {
            switch (currentDimension)
            {
                case Dimension.Second:
                    if (!Vitals.HasPowerUpEnergy)
                        Vitals.ActivateHeatUpRoutine();
                    break;
                case Dimension.First:
                    Vitals.ActivateCoolDownRoutine();
                    break;
            }
        }

        public void Knockback(Vector2 positionOther)
        {
            if (!Vitals.IsInvincible)
                Mover.ReceiveKnockback(positionOther);
        }

        // Author : Mickael Chabot
        public void Heal(float healthPoint)
        {
            Vitals.Heal(healthPoint);
        }

        // Author : Mickael Chabot
        public void OnUsedPowerUpEnergy(float secondsOfInfiniteEnergy)
        {
            Vitals.HasPowerUpEnergy = true;
            Vitals.StopHeatUpRoutine();
            StartCoroutine(Vitals.InfiniteEnergyRoutine(secondsOfInfiniteEnergy));
        }

        // Author : Mickael Chabot
        public void OnStopPowerUpEnergy()
        {
            Vitals.HasPowerUpEnergy = false;
            if (dimensionChangeController.CurrentDimension == Dimension.Second)
            {
                Vitals.StopCoolDownRoutine();
                Vitals.ActivateHeatUpRoutine();
            }
        }
    }
}