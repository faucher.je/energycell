﻿using Harmony;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Game
{
    
    public class Taser : MonoBehaviour
    {

        //This is not a prefab since we re-use an object in the game and not create it.
        [SerializeField] private TaserProjectile projectile;
        [SerializeField] private float heatGainedPerShot = 10;
        [SerializeField] private Transform weaponPosition;
        
        private InputActions inputs;
        private Player player;

        //Author: Samuel Ménard
        private void Awake()
        {
            player = GetComponent<Player>();
            inputs = Finder.Inputs.Actions;
        }

        //Author: Samuel Ménard
        private void OnEnable()
        {
            inputs.Game.Shoot.performed += OnShoot;
        }

        //Author: Samuel Ménard
        private void OnDisable()
        {
            inputs.Game.Shoot.performed -= OnShoot;
        }
        

        //Author: Samuel Ménard && Mickael Chabot ( player.HasTaserGadget )
        private void OnShoot(InputAction.CallbackContext context)
        {
            if (player.HasTaserGadget && !projectile.isActiveAndEnabled)
            {
                player.HeatUp(heatGainedPerShot);
                projectile.gameObject.SetActive(true);
                projectile.transform.position = weaponPosition.position;
            }
        }
    }
}