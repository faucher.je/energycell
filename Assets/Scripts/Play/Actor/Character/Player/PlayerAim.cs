﻿using Harmony;
using UnityEditor;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Game
{
    //Author : Samuel Ménard
    [Findable(Tags.Player)]
    public class PlayerAim : MonoBehaviour
    {
        
        private new Camera camera;
        private Inputs inputs;
        private Vector2 cursorPosition;

        public Vector2 AimingDirection { get; private set; }
        
        private void Awake()
        {
            camera = Camera.main;
            inputs = Finder.Inputs;
            AimingDirection = Vector2.right;
            cursorPosition = Vector2.right;
        }

        private void OnEnable()
        {
            inputs.Actions.Game.Aim.performed += OnAim;
        }
        
        private void Update()
        {
            if (!inputs.Actions.Game.Aim.triggered)
            {
                //If the mouse isn't used and it's keyboard and mouse scheme, we take last known position of mouse in game as aim.
                if (inputs.CurrentControlScheme == inputs.Actions.KeyboardandmouseScheme)
                {
                    AimingDirection = transform.InverseTransformPoint(camera.ScreenToWorldPoint(cursorPosition));
                    AimingDirection = new Vector2(AimingDirection.x * transform.localScale.x,
                        AimingDirection.y * transform.localScale.y).normalized;
                }
            }
        }
        
        private void OnDisable()
        {
            inputs.Actions.Game.Aim.performed -= OnAim;
        }

        private void OnAim(InputAction.CallbackContext context)
        {
            if(inputs.CurrentControlScheme == inputs.Actions.KeyboardandmouseScheme)
            {
                cursorPosition = context.ReadValue<Vector2>();
                AimingDirection = transform.InverseTransformPoint(camera.ScreenToWorldPoint(cursorPosition));
                AimingDirection = new Vector2(AimingDirection.x * transform.localScale.x,
                    AimingDirection.y * transform.localScale.y).normalized;
            }
            else if (inputs.CurrentControlScheme == inputs.Actions.GamepadScheme)
            {
                if(context.ReadValue<Vector2>() == Vector2.zero)
                    AimingDirection = transform.right;
                else
                    AimingDirection = context.ReadValue<Vector2>().normalized;
            }
        }

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            // Aiming vector
            if (EditorApplication.isPlaying)
            {
                GizmosExtensions.DrawLine((Vector2) transform.position, (Vector2)transform.position + AimingDirection, Color.red);
            }
        }
#endif
        
    }
}