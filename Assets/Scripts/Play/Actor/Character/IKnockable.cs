﻿using UnityEngine;

namespace Game
{
    // Author : Samuel Ménard
    public interface IKnockable
    {
        void Knockback(Vector2 positionOther);
    }
}