﻿using UnityEngine;

namespace Game
{
    // Author: Jérémy Faucher
    // Les éléments qui héritent de ceci doivent hériter de MonoBehaviour puisqu'ils iront dans des SerializeField
    public abstract class TriggerTarget : MonoBehaviour
    {
        public abstract void Activate();

        public abstract void Deactivate();
    }
}