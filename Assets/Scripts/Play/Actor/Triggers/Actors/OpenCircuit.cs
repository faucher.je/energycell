﻿using UnityEngine;

namespace Game
{
    //Author: Jérémy Faucher
    [RequireComponent(typeof(BoxCollider2D))]
    public class OpenCircuit : Trigger
    {
        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.GetComponent<TaserProjectile>() != null)
            {
                OnTriggered();
            }
        }
    }
}