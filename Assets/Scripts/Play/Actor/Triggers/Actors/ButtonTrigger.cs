﻿using Harmony;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Game
{
    // Author: Jérémy Faucher
    public class ButtonTrigger : Trigger
    {
        [SerializeField] private float maxDistanceToInteract = 1.5f;
        
        private Player player;
        private InputActions.GameActions inputs;
        private bool hasBeenTriggered;

        private void Awake()
        {
            player = Finder.Player;
            inputs = Finder.Inputs.Actions.Game;
            hasBeenTriggered = false;
        }

        private void OnEnable()
        {
            inputs.Interact.performed += OnInteractTriggered;
        }

        private void OnDisable()
        {
            inputs.Interact.performed -= OnInteractTriggered;
        }

        private void OnInteractTriggered(InputAction.CallbackContext context)
        {
            if (IsPlayerCloseEnough() && !hasBeenTriggered)
            {
                OnTriggered();
                hasBeenTriggered = true;
            }
        }

        private bool IsPlayerCloseEnough()
        {
            return Vector3.Distance(player.transform.position, gameObject.transform.position) <= maxDistanceToInteract;
        }

        public void Reset()
        {
            hasBeenTriggered = false;
            OnUntriggered();
        }
    }
}