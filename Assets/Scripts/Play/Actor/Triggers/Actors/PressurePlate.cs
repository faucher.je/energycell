﻿using UnityEngine;

namespace Game
{
    // Author: Jérémy Faucher
    [RequireComponent(typeof(BoxCollider2D))]
    public class PressurePlate: Trigger
    {
        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.GetComponent<ICharacter>() != null) OnTriggered();
        }

        private void OnCollisionExit2D(Collision2D other)
        {
            if (other.gameObject.GetComponent<ICharacter>() != null) OnUntriggered();
        }
    }
}