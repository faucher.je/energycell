﻿using UnityEngine;

namespace Game
{
    // Author: Jérémy Faucher
    public abstract class Trigger : MonoBehaviour
    {
        [SerializeField] private TriggerTarget[] targets;
        
        protected void OnTriggered()
        {
            foreach (var target in targets)
            {
                target.Activate();
            }
        }

        protected void OnUntriggered()
        {
            foreach (var target in targets)
            {
                target.Deactivate();
            }
        }
    }
}