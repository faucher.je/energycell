﻿using System.Collections;
using UnityEngine;

namespace Game
{
    // Author : Jérémy Faucher
    [RequireComponent(typeof(Collider2D))]
    public class Door : TriggerTarget
    {
        [SerializeField] private GameObject topGameObject;
        [SerializeField] private GameObject bottomGameObject;
        [SerializeField] private int nbTickDoorOpening = 10;
        [SerializeField] private float amountDoorMoveOnTick = 0.15f;
        [SerializeField] private float doorOpeningTickDelay = 0.0625f;
        
        private PolygonCollider2D polygonCollider2D;
        private Coroutine currentCoroutine;
        private Vector3 origPositionTop;
        private Vector3 origPositionBottom;
        
        private void Awake()
        {
            polygonCollider2D = GetComponent<PolygonCollider2D>();
            origPositionTop = topGameObject.transform.position;
            origPositionBottom = bottomGameObject.transform.position;
        }

        [ContextMenu("Activate")]
        public override void Activate()
        {
            currentCoroutine = StartCoroutine(OpenDoor());
        }

        [ContextMenu("Deactivate")]
        public override void Deactivate()
        {
            if (currentCoroutine != null) StopCoroutine(currentCoroutine);
            topGameObject.transform.position = origPositionTop;
            bottomGameObject.transform.position = origPositionBottom;
            polygonCollider2D.enabled = true;
        }

        // Pour que la porte disparaisse comme il faut, il faut ajouter un Sprite Mask dans la scène.
        // Il ne suffit que mettre le mask pour qu'il enveloppe la porte lorsqu'elle est fermée pour
        // que celle-ci disparaisse lorsqu'elle s'ouvre.
        // Voir dans le niveau 6 pour un exemple dans la première dimension.
        private IEnumerator OpenDoor()
        {
            for (int i = 0; i < nbTickDoorOpening; i++)
            {
                topGameObject.transform.Translate(Vector2.up * (amountDoorMoveOnTick * Time.deltaTime));
                topGameObject.transform.position += new Vector3(0, amountDoorMoveOnTick, 0);
                bottomGameObject.transform.position += new Vector3(0, -amountDoorMoveOnTick, 0);
                yield return new WaitForSeconds(doorOpeningTickDelay);
            }

            polygonCollider2D.enabled = false;
        }
    }
}