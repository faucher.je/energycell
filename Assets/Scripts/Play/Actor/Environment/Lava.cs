﻿using UnityEngine;

namespace Game
{
    // Author : Rebecca Brassard
    public class Lava : MonoBehaviour
    {

        private void OnTriggerEnter2D(Collider2D other)
        {
            IKillable killable = other.gameObject.GetComponent<IKillable>();

            if (killable != null) killable.Kill();
        }
    }
}