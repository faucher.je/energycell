﻿using UnityEngine;

namespace Game
{
    // Author : Samuel Ménard
    public class Spike : MonoBehaviour
    {

        [SerializeField] private float damage;
        
        private ISensor<ICharacter> sensor;

        private void Awake()
        {
            sensor = GetComponentInChildren<Sensor>().For<ICharacter>();
        }
        
        private void OnEnable()
        {
            sensor.OnSensedObject += OnSensedObject;
            sensor.OnUnsensedObject += OnUnsensedObject;
        }
        
        private void OnDisable()
        {
            sensor.OnSensedObject -= OnSensedObject;
            sensor.OnUnsensedObject -= OnUnsensedObject;
        }
        
        private void OnSensedObject(ICharacter character)
        {
            character.Knockback(transform.position);
            character.Hurt(damage);
        }

        //As useless as this function looks like, it is needed so the sensor
        //subscribes to the OnUnsensedObjectInternal event to make everything works. Approved by Ben.
        private void OnUnsensedObject(ICharacter character)
        {
            
        }
    }
}