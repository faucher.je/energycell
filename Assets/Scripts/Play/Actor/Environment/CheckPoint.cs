﻿using UnityEngine;

namespace Game
{
    //Author: Samuel Ménard
    public class CheckPoint : MonoBehaviour
    {
        //TODO: Change this to a better way
        [SerializeField] private Resettable[] elementsToReset;
        
        private ISensor<Player> sensor;

        public Vector3 SpawnPosition { get; set; }

        private void Awake()
        {
            sensor = this.GetRequiredComponentInChildren<Sensor>().For<Player>();
            Vector3 tempSpawnPosition = gameObject.transform.position;
            //Removed half of spawnPoint size so player's feet is at the bottom of the spawn point.
            tempSpawnPosition.y -= GetComponentInChildren<BoxCollider2D>().bounds.size.y/2;
            SpawnPosition = tempSpawnPosition;
        }
        

        private void OnEnable()
        {
            sensor.OnSensedObject += OnSensedObject;
            sensor.OnUnsensedObject += OnUnsensedObject;
        }
        
        private void OnDisable()
        {
            sensor.OnSensedObject -= OnSensedObject;
            sensor.OnUnsensedObject -= OnUnsensedObject;
        }
        
        private void OnSensedObject(Player player)
        {
            player.CurrentCheckPoint = this;
            //We deactivate the script since the checkpoint is used once.
            enabled = false;
        }

        //As useless as this function looks like, it is needed so the sensor
        //subscribes to the OnUnsensedObjectInternal event to make everything work. Approved by Ben
        private void OnUnsensedObject(Player player)
        {
            
        }

        // Author: Jérémy Faucher
        public void ResetElements()
        {
            foreach (var element in elementsToReset)
            {
                element.ResetToOriginalState();
            }
        }
    }
}