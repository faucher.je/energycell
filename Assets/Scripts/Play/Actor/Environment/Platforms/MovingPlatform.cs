﻿using UnityEngine;

namespace Game
{
    //Author: Dave Ouellet
    public abstract class MovingPlatform: MonoBehaviour
    {
        //Le joueur ou l'ennemi qui entre sur une plateforme va suivre le mouvement de la plateforme
        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.CompareTag("Player")|| other.gameObject.CompareTag("Enemy") )
            {
                other.transform.parent = transform;
            }
        }
        //Le joueur ou l'ennemi qui débarque d'une plateforme ne suit plus le mouvement de la plateforme
        private void OnCollisionExit2D (Collision2D other) {
            if (other.gameObject.CompareTag("Player") || other.gameObject.CompareTag("Enemy") )
            {
                other.transform.parent = null;
            }
        }
    }
}