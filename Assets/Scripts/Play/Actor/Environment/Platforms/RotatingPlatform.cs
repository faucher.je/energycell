﻿
using UnityEngine;

namespace Game
{
    //Author: Dave Ouellet
    public class RotatingPlatform : MovingPlatform
    {
        [SerializeField] private Transform rotationCenter;
        [SerializeField] private float rotationRadius = 2f;
        [SerializeField] private float rotationSpeed = 2f;

        private float newPosX;
        private float newPosY; 
        private float angle = 0f;

        private void Update()
        {
            //Déplace la plateforme autour de la position du centre de rotation
            var position = rotationCenter.position;
            newPosX = position.x + Mathf.Cos(angle) * rotationRadius;
            newPosY = position.y + Mathf.Sin(angle) * rotationRadius;
            transform.position = new Vector2(newPosX,newPosY);
            angle += Time.deltaTime * rotationSpeed;
            if (angle >= 360f)
                angle = 0f;
        }
    }
}