﻿using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Game
{
    // Author: Jérémy Faucher
    [RequireComponent(typeof(BoxCollider2D))]
    [RequireComponent(typeof(SpriteRenderer))]
    public class FallingPlatform : Resettable
    {
        [SerializeField] private float timeToFall = 2f;
        [SerializeField] private float timeToRespawn = 5f;
        [SerializeField] private float shakeSpeed = 2f;
        [SerializeField] private float shakeAmount = 0.005f;

        private bool isWeakening;
        private float nbOnPlatform;
        private Coroutine currentWeakenCoroutine;
        private SpriteRenderer spriteRenderer;
        private BoxCollider2D boxCollider2D;
        private Vector3 respawnPosition;

        protected override void Awake()
        {
            base.Awake();
            isWeakening = false;
            nbOnPlatform = 0;
            spriteRenderer = GetComponent<SpriteRenderer>();
            boxCollider2D = GetComponent<BoxCollider2D>();
            respawnPosition = transform.position;
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.GetComponent<ICharacter>() != null)
            {
                if (nbOnPlatform <= 0)
                {
                    currentWeakenCoroutine = StartCoroutine(Weaken());
                }
                nbOnPlatform++;
            }
        }

        private void OnCollisionExit2D(Collision2D other)
        {
            nbOnPlatform = Mathf.Clamp(nbOnPlatform - 1, 0, float.MaxValue);
            if (nbOnPlatform <= 0)
            {
                StopCoroutine(currentWeakenCoroutine);
                isWeakening = false;
            }
        }

        private void Update()
        {
            if (isWeakening)
            {
                //TODO: Remplacer par une animation de shake
                transform.position = transform.position +
                                     new Vector3((shakeSpeed - Time.deltaTime) * Random.Range(-shakeAmount, shakeAmount), 0);
            }
        }

        private IEnumerator Weaken()
        {
            isWeakening = true;
            yield return new WaitForSeconds(timeToFall);
            StartCoroutine(Respawn());
            Deactivate();
        }

        private IEnumerator Respawn()
        {
            yield return new WaitForSeconds(timeToRespawn);
            Activate();
            
        }
        
        private void Activate()
        {
            boxCollider2D.enabled = true;
            spriteRenderer.enabled = true;
            isWeakening = false;
            transform.position = respawnPosition;
        }

        private void Deactivate()
        {
            boxCollider2D.enabled = false;
            spriteRenderer.enabled = false;
        }

        public override void ResetToOriginalState()
        {
            base.ResetToOriginalState();
            Activate();
        }
    }
}