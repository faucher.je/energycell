﻿using System;
using UnityEngine;

namespace Game
{
    //Author: Dave Ouellet
    public class TranslatingPlatform : MovingPlatform
    {
        [SerializeField] public float translationSpeed = 2;
        [SerializeField] private float translationDistance = 6;
        [SerializeField] private PlatformMovementDirection movementDirection = PlatformMovementDirection.Lateral;
        private Vector2 positiveDirection;
        private Vector2 negativeDirection;
        private float axisPosition;
        private float initialAxisPosition;
        private bool reachedNegativeLimit;
        private bool reachedPositiveLimit;

        private void Awake()
        {
            if (movementDirection == PlatformMovementDirection.Lateral)
            {
                initialAxisPosition = transform.position.x;
                positiveDirection = Vector2.right;
                negativeDirection = Vector2.left;
            }
            else
            {
                initialAxisPosition = transform.position.y;
                positiveDirection = Vector2.up;
                negativeDirection = Vector2.down;
            }
        }

        private void Update()
        {
            var position = transform.position;
            axisPosition = movementDirection == PlatformMovementDirection.Lateral ? position.x : position.y;

            //Déplace la plateforme jusqu'à ce qu'elle atteigne la limite définie dans translation radius
            if (!reachedPositiveLimit)
            {
                transform.Translate(positiveDirection * (translationSpeed * Time.deltaTime));
                if (!(axisPosition >= initialAxisPosition + translationDistance)) return;
                reachedPositiveLimit = true;
                reachedNegativeLimit = false;
            }
            
            else if (!reachedNegativeLimit)
            {
                transform.Translate(negativeDirection * (translationSpeed * Time.deltaTime));
                if (!(axisPosition <= initialAxisPosition - translationDistance)) return;
                reachedPositiveLimit = false;
                reachedNegativeLimit = true;
            }
        }
    }

    public enum PlatformMovementDirection
    {
        Lateral,
        Vertical
    }
}