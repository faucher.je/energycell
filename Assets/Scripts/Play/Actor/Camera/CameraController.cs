﻿using Cinemachine;
using Harmony;
using UnityEngine;

namespace Game
{
    
    [RequireComponent(typeof(CinemachineConfiner))] [Findable(Tags.GameCamera)]
    public class CameraController : MonoBehaviour
    {
        [SerializeField] private CompositeCollider2D[] cameraConfiners;
        [SerializeField] private Player player;
        
        private readonly Vector3 TRANSLATE_TO_FIRST_DIMENSION = new Vector3(0, -500,0);
        private readonly Vector3 TRANSLATE_TO_SECOND_DIMENSION = new Vector3(0, 500,0);
        
        private DimensionChangeEventChannel dimensionChangeEventChannel;
        private CinemachineConfiner cinemachineConfiner;

        
        //Author: Jérémy Faucher & Mickael Chabot
        private void Awake()
        {
            dimensionChangeEventChannel = Finder.DimensionChangeEventChannel;
            cinemachineConfiner = GetComponent<CinemachineConfiner>();

            dimensionChangeEventChannel.OnDimensionChange += OnDimensionChange;
        }

        //Author: Jérémy Faucher
        private void OnDestroy()
        {
            dimensionChangeEventChannel.OnDimensionChange -= OnDimensionChange;
        }

        //Author: Jérémy Faucher
        private static CompositeCollider2D GetCameraConfiner(string tag)
        {
            return GameObject.FindWithTag(tag).GetComponent<CompositeCollider2D>();
        }

        //Author: Jérémy Faucher & Mickael Chabot
        private void OnDimensionChange(Dimension newDimension)
        {
            switch (newDimension)
            {
                case Dimension.First:
                    MoveConfinersToFirstDimension();
                    break;
                case Dimension.Second:
                    MoveConfinersToSecondDimension();
                    break;
            }
        }

        // Author : Mickael Chabot
        private void Update()
        {
            for (int i = 0; i < cameraConfiners.Length; i++)
            {
                if (cameraConfiners[i].bounds.Contains(player.transform.position))
                {
                    cinemachineConfiner.m_BoundingShape2D = cameraConfiners[i];
                }
            }
        }

        // Author : Mickael Chabot
        private void MoveConfinersToSecondDimension()
        {
            for (int i = 0; i < cameraConfiners.Length; i++)
            {
                cameraConfiners[i].transform.Translate(TRANSLATE_TO_SECOND_DIMENSION);
            }
        }

        // Author : Mickael Chabot
        private void MoveConfinersToFirstDimension()
        {
            for (int i = 0; i < cameraConfiners.Length; i++)
            {
                cameraConfiners[i].transform.Translate(TRANSLATE_TO_FIRST_DIMENSION);
            }
        }
    }
}