﻿using Cinemachine;
using Harmony;
using UnityEngine;

namespace Game
{
    
    public class CameraZoomController : MonoBehaviour
    {
        [SerializeField] private Transform zoomInTransform;
        [SerializeField] private float cameraZoomStep = 0.00075f;
        [SerializeField] private float cameraMaxZoomSize = 2.5f;
        
        private CinemachineConfiner cinemachineConfiner;
        private CinemachineVirtualCamera virtualCamera;
        private Transform originalFollow;
        private Transform originalLookAt;
        private float originalOrthographicSize;
        private float cameraDistance;
        private bool isZoomingIn;
        private bool isZoomingOut;
        
        // Author : Mickael Chabot
        private void Awake()
        {
            virtualCamera = GetComponent<CinemachineVirtualCamera>();
            
            originalFollow = virtualCamera.Follow;
            originalLookAt = virtualCamera.LookAt;
            originalOrthographicSize = virtualCamera.m_Lens.OrthographicSize;
        }

        // Author : Mickael Chabot
        private void Start()
        {
            if (zoomInTransform != null)
            {
                virtualCamera.Follow = zoomInTransform;
                virtualCamera.LookAt = zoomInTransform;
                
                isZoomingIn = true;
            }
        }
        
        // Author : Mickael Chabot
        private void Update()
        {
            if (isZoomingIn)
            {
                virtualCamera.m_Lens.OrthographicSize -= cameraZoomStep * Time.deltaTime;
                if (virtualCamera.m_Lens.OrthographicSize <= cameraMaxZoomSize)
                {
                    isZoomingIn = false;
                    isZoomingOut = true;
                }
            }
            else if (isZoomingOut)
            {
                virtualCamera.m_Lens.OrthographicSize += cameraZoomStep * Time.deltaTime;
                if (virtualCamera.m_Lens.OrthographicSize >= originalOrthographicSize)
                {
                    isZoomingOut = false;
                    virtualCamera.Follow = originalFollow;
                    virtualCamera.LookAt = originalLookAt;
                    virtualCamera.m_Lens.OrthographicSize = originalOrthographicSize;
                }
            }
        }
    }
}