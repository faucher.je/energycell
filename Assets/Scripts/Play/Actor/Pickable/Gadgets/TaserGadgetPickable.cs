﻿using Game.Pickable;
using UnityEngine;

namespace Game
{
    
    public class TaserGadgetPickable : MonoBehaviour, IPickable
    {
        private ISensor<Player> sensor;

        // Author : Mickael Chabot
        private void Awake()
        {
            sensor = this.GetRequiredComponentInChildren<Sensor>().For<Player>();
        }
        
        // Author : Mickael Chabot
        private void OnEnable()
        {
            sensor.OnSensedObject += OnSensedObject;
        }
        
        // Author : Mickael Chabot
        private void OnDisable()
        {
            sensor.OnSensedObject -= OnSensedObject;
        }
        
        // Author : Mickael Chabot
        private void OnSensedObject(Player player)
        {
            player.HasTaserGadget = true;
            Destroy(gameObject);
        }
    }
}