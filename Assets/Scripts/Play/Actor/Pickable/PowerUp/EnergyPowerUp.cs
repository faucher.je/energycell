﻿using Game.Pickable;
using UnityEngine;

namespace Game
{
    public class EnergyPowerUp : MonoBehaviour, IPickable
    {
        [SerializeField] private float secondsOfInfiniteEnergy = 10;
        
        private ISensor<Player> sensor;

        // Author : Mickael Chabot
        private void Awake()
        {
            sensor = this.GetRequiredComponentInChildren<Sensor>().For<Player>();
        }

        // Author : Mickael Chabot
        private void OnEnable()
        {
            sensor.OnSensedObject += OnSensedObject;
        }
        
        // Author : Mickael Chabot
        private void OnDisable()
        {
            sensor.OnSensedObject -= OnSensedObject;
        }
        
        // Author : Mickael Chabot
        private void OnSensedObject(Player player)
        {
            player.OnUsedPowerUpEnergy(secondsOfInfiniteEnergy);
            Destroy(gameObject);
        }
    }
}