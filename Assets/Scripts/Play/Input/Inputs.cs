using Harmony;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Users;

namespace Game
{
    // Author : Samuel Ménard
    [Findable(Tags.MainController)]
    public class Inputs : MonoBehaviour
    {   
        private InputActions actions;
        
        public InputActions Actions => actions ?? (actions = new InputActions());
        public InputControlScheme CurrentControlScheme { get; private set; }

        private void Awake()
        {
            CurrentControlScheme = Actions.KeyboardandmouseScheme;
        }

        private void OnEnable()
        {
            InputUser.onChange += OnInputDeviceChanged;
        }

        private void OnDisable()
        {
            InputUser.onChange -= OnInputDeviceChanged;
        }

        private void OnInputDeviceChanged(InputUser user, InputUserChange change, InputDevice device)
        {
            if (change == InputUserChange.ControlSchemeChanged)
            {
                if (user.controlScheme != null) CurrentControlScheme = user.controlScheme.Value;
            }
        }
    }
}