﻿using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    //Author: Jérémy Faucher
    public class BossHealthBar : MonoBehaviour
    {
        private Image bossHealthBarSprite;
        private BossHitEventChannel bossHitEventChannel;
        private Boss boss;

        private void Awake()
        {
            bossHealthBarSprite = this.GetRequiredComponent<Image>();
            bossHitEventChannel = Finder.BossHitEventChannel;
            boss = Finder.Boss;
        }

        private void OnEnable()
        {
            bossHitEventChannel.OnBossHit += OnBossHit;
        }

        private void OnDisable()
        {
            bossHitEventChannel.OnBossHit -= OnBossHit;
        }

        private void OnBossHit(float health)
        {
            bossHealthBarSprite.fillAmount = health / boss.BossVitals.MaxBossHealth;
        }
    }
}