﻿using System;
using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class HealthBar : MonoBehaviour
    {
        private Image healthBarSprite;
        private PlayerHealthChangeEventChannel playerHealthChangeEventChannel;
        private Player player;
        
        // Author : Mickael Chabot
        private void Awake()
        {
            healthBarSprite = this.GetRequiredComponent<Image>();
            playerHealthChangeEventChannel = Finder.PlayerHealthChangeEventChannel;
            player = Finder.Player;
        }

        // Author : Mickael Chabot
        private void OnEnable()
        {
            playerHealthChangeEventChannel.OnPlayerHealthChanged += OnPlayerHealthChange;
            
        }
        
        // Author : Mickael Chabot
        private void Start()
        {
            OnPlayerHealthChange(player.Vitals.CurrentHealth);
        }

        // Author : Mickael Chabot
        private void OnDisable()
        {
            playerHealthChangeEventChannel.OnPlayerHealthChanged -= OnPlayerHealthChange;
        }

        // Author : Mickael Chabot
        private void OnPlayerHealthChange(float playerHealth)
        {
            healthBarSprite.fillAmount = playerHealth / player.Vitals.MaxHealthPoints;
        }
    }
}