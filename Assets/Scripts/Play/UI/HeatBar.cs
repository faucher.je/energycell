﻿using System;
using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class HeatBar : MonoBehaviour
    {
        private Image heatBarSprite;
        private PlayerHeatChangeEventChannel playerHeatChangeEventChannel;
        private Player player;
        
        // Author : Mickael Chabot
        private void Awake()
        {
            heatBarSprite = this.GetRequiredComponent<Image>();
            playerHeatChangeEventChannel = Finder.PlayerHeatChangeEventChannel;
            player = Finder.Player;
            //TODO: Update à la première frame
        }

        // Author : Mickael Chabot
        private void OnEnable()
        {
            playerHeatChangeEventChannel.OnPlayerHeatChange += OnPlayerHeatChange;
        }

        // Author : Mickael Chabot
        private void Start()
        {
            OnPlayerHeatChange(player.Vitals.CurrentHeat);
        }
        
        // Author : Mickael Chabot
        private void OnDisable()
        {
            playerHeatChangeEventChannel.OnPlayerHeatChange -= OnPlayerHeatChange;
        }

        // Author : Mickael Chabot
        private void OnPlayerHeatChange(float playerHeat)
        {
            heatBarSprite.fillAmount = playerHeat / player.Vitals.MaxHeatPoints;
        }
    }
}