﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Mathieu Boutet
    // Takes an already existing Collider2D in the object instead of creating a new one
    [RequireComponent(typeof(Collider2D))]
    public sealed class Stimuli : MonoBehaviour
    {
        public event StimuliEventHandler OnDestroyed;

        private void Awake()
        {
            InitCollider();
            SetSensorLayer();
        }

        private void OnDestroy()
        {
            NotifyDestroyed();
        }

        private void InitCollider()
        {
            gameObject.GetComponent<Collider2D>().isTrigger = true;
        }

        private void SetSensorLayer()
        {
            gameObject.layer = Layers.Sensor;
        }

        private void NotifyDestroyed()
        {
            if (OnDestroyed != null) OnDestroyed(transform.parent.gameObject);
        }
    }

    public delegate void StimuliEventHandler(GameObject otherObject);
}