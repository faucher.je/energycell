﻿namespace Game
{
    //Author : Jeremy Faucher et Dave Ouellet
    public class GameOptionsMenu : Options
    {

        protected override void OnApplyButtonClick()
        {
            base.OnApplyButtonClick();
            gameObject.SetActive(false);
        }

        protected override void OnReturnButtonClick()
        {
            gameObject.SetActive(false);
        }
    }
}