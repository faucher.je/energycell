﻿namespace Game
{
    // Author: Jérémy Faucher
    public class GameAchievementsMenu : AchievementsMenu
    {
        protected override void OnQuitButtonClick()
        {
            gameObject.SetActive(false);
        }
    }
}