﻿using UnityEngine;

namespace Game
{
    // Author: Jérémy Faucher
    public class AchievementsPage : MonoBehaviour
    {
        [SerializeField] private AchievementsPage previousPage;
        [SerializeField] private AchievementsPage nextPage;
        
        public AchievementsPage GoToNextPage()
        {
            nextPage.gameObject.SetActive(true);
            gameObject.SetActive(false);
            return nextPage;
        }
        
        public AchievementsPage GoToPreviousPage()
        {
            previousPage.gameObject.SetActive(true);
            gameObject.SetActive(false);
            return previousPage;
        }
    }
}