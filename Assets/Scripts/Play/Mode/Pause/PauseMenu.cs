﻿using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    // Author: Jérémy Faucher
    public class PauseMenu : MonoBehaviour
    {
        [SerializeField] private GameObject gameOptionsMenu;
        [SerializeField] private GameObject achievementsMenu;
        
        private Button resumeButton;
        private Button quitButton;
        private Button optionsButton;
        private Button achievementsButton;

        private MainMenuSettings mainMenuSettings;
        private AchievementSpeedrunnerEventChannel achievementSpeedrunnerEventChannel;
        
        private Main main;

        //TODO: Implémenter la navigation dans le menu avec les contrôles de menu
        private Inputs inputs;

        private void Awake()
        {
            var buttons = GetComponentsInChildren<Button>();
            resumeButton = buttons.WithName(GameObjects.Resume);
            quitButton = buttons.WithName(GameObjects.Quit);
            achievementsButton = buttons.WithName(GameObjects.Achievements);
            optionsButton = buttons.WithName(GameObjects.Options);

            gameOptionsMenu.SetActive(false);
            achievementsMenu.SetActive(false);
            main = Finder.Main;
            mainMenuSettings = Finder.MainMenuSettings;

            inputs = Finder.Inputs;

            achievementSpeedrunnerEventChannel = Finder.AchievementSpeedrunnerEventChannel;
        }

        private void OnEnable()
        {
            // Devrait mettre le jeu sur pause à revoir si pas tout est sur pause
            Time.timeScale = 0;
            
            inputs.Actions.Game.Disable();
            inputs.Actions.Menu.Enable();
            resumeButton.onClick.AddListener(OnResumeButtonClick);
            quitButton.onClick.AddListener(OnQuitButtonClick);
            achievementsButton.onClick.AddListener(OnAchievementButtonClick);
            optionsButton.onClick.AddListener(OnOptionsButtonClick);
        }

        private void OnDisable()
        {
            resumeButton.onClick.RemoveListener(OnResumeButtonClick);
            quitButton.onClick.RemoveListener(OnQuitButtonClick);
            achievementsButton.onClick.RemoveListener(OnAchievementButtonClick);
            optionsButton.onClick.RemoveListener(OnOptionsButtonClick);
            inputs.Actions.Menu.Disable();
            Time.timeScale = 1;
        }

        private void OnResumeButtonClick()
        {
            // Pas dans OnDisable, car cela serait appelé lorsque nous voulons aller au menu principal 
            inputs.Actions.Game.Enable();
            main.UnloadPauseMenuScenes();
        }

        private void OnQuitButtonClick()
        {
            achievementSpeedrunnerEventChannel.Publish(false);
            main.UnloadPauseMenuScenes();
            mainMenuSettings.FirstPageToLoad = MainMenuPage.HomePage;
            main.GameState = GameState.Menu;
        }

        private void OnAchievementButtonClick()
        {
            gameOptionsMenu.SetActive(false);
            // Si le menu est ouvert cela le ferme  et vice-versa
            achievementsMenu.SetActive(!achievementsMenu.activeSelf);
        }

        private void OnOptionsButtonClick()
        {
            achievementsMenu.SetActive(false);
            gameOptionsMenu.SetActive(!gameOptionsMenu.activeSelf);
        }
    }
}