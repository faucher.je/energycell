﻿using System;
using Game;
using Harmony;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Play
{
    // Author: Jérémy Faucher
    public class GameEndController : MonoBehaviour
    {
        private GameObject congratulationScreen;
        private GameObject creditScreen;
        private InputActions input;
        private Main main;
        private void Awake()
        {
            input = Finder.Inputs.Actions;
            main = Finder.Main;
            var childrens = gameObject.Children();

            foreach (var children in childrens)
            {
                if (children.name == GameObjects.CongratulationScreen)
                {
                    congratulationScreen = children;
                }
                else if (children.name == GameObjects.CreditsScreen)
                {
                    creditScreen = children;
                }
            }
        }

        private void OnEnable()
        {
            input.Game.Disable();
            input.Menu.Enable();
            congratulationScreen.SetActive(true);
            creditScreen.SetActive(false);
            input.Menu.Submit.performed += OnNextPage;
        }

        private void OnDisable()
        {
            input.Menu.Submit.performed -= OnNextPage;
        }

        private void OnNextPage(InputAction.CallbackContext context)
        {
            if (!creditScreen.activeSelf)
            {
                congratulationScreen.SetActive(false);
                creditScreen.SetActive(true);
            }
            else
            {
                main.UnloadGameEndScenes();
                main.GameState = GameState.Menu;
            }
        }
    }
}