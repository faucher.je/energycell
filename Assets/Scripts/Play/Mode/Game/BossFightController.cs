﻿using System;
using Harmony;
using UnityEngine;

namespace Game
{
    public class BossFightController : MonoBehaviour
    {
        private Main main;
        private BossHitEventChannel bossHitEventChannel;

        private void Awake()
        {
            main = Finder.Main;
            bossHitEventChannel = Finder.BossHitEventChannel;
        }

        private void OnEnable()
        {
            bossHitEventChannel.OnBossHit += OnBossHit;
        }

        private void OnDisable()
        {
            bossHitEventChannel.OnBossHit -= OnBossHit;
        }

        private void OnBossHit(float healthRemaining)
        {
            if (healthRemaining <= 0)
            {
                main.LoadGameEndScenes();
            }
        }
    }
}