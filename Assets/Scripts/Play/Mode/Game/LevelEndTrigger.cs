﻿using Harmony;
using UnityEngine;

namespace Game
{
    
    public class LevelEndTrigger : MonoBehaviour
    {
        private ISensor<Player> sensor;
        private LevelController levelController;
        private LevelEndEventChannel levelEndEventChannel;
        
        // Author : Mickael Chabot
        private void Awake()
        {
            sensor = this.GetRequiredComponentInChildren<Sensor>().For<Player>();
            levelEndEventChannel = Finder.LevelEndEventChannel;
            levelController = Finder.LevelController;

        }
        
        // Author : Mickael Chabot
        private void OnEnable()
        {
            sensor.OnSensedObject += OnSensedObject;
        }
        
        // Author : Mickael Chabot
        private void OnDisable()
        {
            sensor.OnSensedObject -= OnSensedObject;
        }
        
        // Author : Mickael Chabot
        private void OnSensedObject(Player player)
        {
            levelEndEventChannel.Publish(levelController.CurrentLevel);
            levelController.LoadNextLevel();
        }
    }
}