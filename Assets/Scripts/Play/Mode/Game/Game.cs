using Harmony;
using UnityEngine;

namespace Game
{
    // Author: Jérémy Faucher
    [Findable(Tags.GameController)]
    public class Game : MonoBehaviour
    {
        private Main main;
        private InputActions.GameActions inputs;

        private void Awake()
        {
            inputs = Finder.Inputs.Actions.Game;
            main = Finder.Main;
        }

        private void Update()
        {
            if (inputs.Pause.triggered)
            {
                PauseGame();
            }
        }

        [ContextMenu("Pause")]
        private void PauseGame()
        {
            main.LoadPauseMenuScenes();
        }

        [ContextMenu("Tutorial")]
        private void TutorialPause()
        {
            main.LoadTutorialMenuScenes();
        }
    }
}