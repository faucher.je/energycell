﻿using System.Collections;
using Harmony;
using UnityEngine;
using UnityEngine.InputSystem;


namespace Game
{

    [Findable(Tags.GameController)]
    public class DimensionChangeController : MonoBehaviour
    {
        [SerializeField] private float distanceBetweenDimensions = 500f;
        [SerializeField] private float cooldownBetweenDimensionChanges = 0.5f;

        private DimensionChangeEventChannel dimensionChangeEventChannel;
        private InputActions.GameActions inputs;
        private bool dimensionChangeWaitIsOver;
        private Dimension currentDimension;
        private Player player;

        public Dimension CurrentDimension
        {
            get => currentDimension;
            private set
            {
                if (value == currentDimension) return;
                
                currentDimension = value;
                dimensionChangeEventChannel.Publish(value);
            }
        }

        public float DistanceBetweenDimensions => distanceBetweenDimensions;

        //Author: Dave Ouellet
        private void Awake()
        {
            player = Finder.Player;
            inputs = Finder.Inputs.Actions.Game;
            dimensionChangeEventChannel = Finder.DimensionChangeEventChannel;
            dimensionChangeWaitIsOver = true;
        }

        //Author: Dave Ouellet
        private void OnEnable()
        {
            inputs.ChangeDimension.started += ChangeDimension;
        }

        //Author: Dave Ouellet
        private IEnumerator ChangeDimensionRoutine()
        {
            dimensionChangeWaitIsOver = false;
            //Si le joueur est dans la dimension 1, il monte
            if (CurrentDimension == Dimension.First)
            {
                CurrentDimension = Dimension.Second;
            }
            //Si le joueur est dans la dimension 2, il redescend
            else
            {
                CurrentDimension = Dimension.First;
            }

            yield return new WaitForSeconds(cooldownBetweenDimensionChanges);
            dimensionChangeWaitIsOver = true;
        }

        //Author: Dave Ouellet
        private void OnDisable()
        {
            inputs.ChangeDimension.started -= ChangeDimension;
        }

        //Author: Dave Ouellet && Mickael Chabot
        private void ChangeDimension(InputAction.CallbackContext context)
        {
            if (player.HasDimensionGadget && dimensionChangeWaitIsOver)
            {
                StartCoroutine(ChangeDimensionRoutine());
            }
        }

        // Author : Mickael Chabot
        public void ResetDimensionToFirstDimension()
        {
            CurrentDimension = Dimension.First;
        }
    }

    public enum Dimension
    {
        First,
        Second
    }
}