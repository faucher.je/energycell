﻿namespace Game
{
    // Author : Mickael Chabot
    public enum Level
    {
        Level1 = 0,
        Level2 = 1,
        Level3 = 2,
        Level4 = 3,
        Level5 = 4,
        Level6 = 5
    }
}