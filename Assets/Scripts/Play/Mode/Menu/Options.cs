﻿using System.Collections.Generic;
using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    // Author: Jérémy Faucher et Dave Ouellet
    public abstract class Options : MonoBehaviour
    {
        [SerializeField] private Button returnButton;
        [SerializeField] private Button saveButton;
        [SerializeField] private Slider soundSlider;
        [SerializeField] private Slider musicSlider;
        [SerializeField] private Dropdown resolutionDropdown;
        [SerializeField] private Toggle fullScreenToggle;
        [SerializeField] private Toggle tutorialsToggle;
        [SerializeField] private string format;
        private TutorialController tutorialController;
        private Resolution[] resolutions;

        public virtual void Awake()
        {
            var sliders = GetComponentsInChildren<Slider>();
            var dropdowns = GetComponentsInChildren<Dropdown>();
            var toggles = GetComponentsInChildren<Toggle>();
            Button[] buttons = GetComponentsInChildren<Button>();
            soundSlider = sliders.WithName(GameObjects.SoundSlider);
            musicSlider = sliders.WithName(GameObjects.MusicSlider);
            resolutionDropdown = dropdowns.WithName(GameObjects.ResolutionDropdown);
            fullScreenToggle = toggles.WithName(GameObjects.FullScreenToggle);
            saveButton = buttons.WithName(GameObjects.SaveButton);
            returnButton = buttons.WithName(GameObjects.ExitButton);
            soundSlider.value = AudioListener.volume;
            tutorialsToggle = toggles.WithName(GameObjects.TutorialsEnabledToggle);
            tutorialController = Finder.TutorialController;
            tutorialsToggle.isOn = tutorialController.TutorialsAreEnabled;
            SetResolutionDropdown();
        }

        public void OnEnable()
        {
            saveButton.onClick.AddListener(OnApplyButtonClick);
            returnButton.onClick.AddListener(OnReturnButtonClick);
        }
        
        public void OnDisable()
        {
            saveButton.onClick.RemoveListener(OnApplyButtonClick);
            returnButton.onClick.RemoveListener(OnReturnButtonClick);
        }
        
        private void SetResolutionDropdown()
        {
            resolutions = Screen.resolutions;
            List<string> options = new List<string>();
            foreach (Resolution resolution in resolutions)
            {
                var option = string.Format(format, resolution.width, resolution.height);
                options.Insert(0, option);
            }
            resolutionDropdown.ClearOptions();
            resolutionDropdown.AddOptions(options);
            fullScreenToggle.isOn = Screen.fullScreen;
            //TODO: Mettre la valeur qui est déjà selectionné de la musique
        }

        protected virtual void OnApplyButtonClick()
        {
            var res = resolutionDropdown.options[resolutionDropdown.value].text.Split('x');
            var width = int.Parse(res[0]);
            var height = int.Parse(res[1]);

            foreach (var resolution in resolutions)
            {
                if (width != resolution.width || height != resolution.height) continue;
                Screen.SetResolution(resolution.width, resolution.height, fullScreenToggle.isOn ? FullScreenMode.ExclusiveFullScreen : FullScreenMode.Windowed);
            }

            AudioListener.volume = soundSlider.value;
            // TODO: Ajuster le son de la musique avec la valeur du slider associé
        }

        protected abstract void OnReturnButtonClick();
    }
    
}