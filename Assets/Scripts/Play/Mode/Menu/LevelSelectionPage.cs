﻿using System.Collections.Generic;
using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    //Author : Samuel Ménard
    public class LevelSelectionPage : Page
    {
        private Main main;
        private LevelController levelController;
        private Button returnButton;
        private List<Button> levelButtons;
        
        private void Awake()
        { 
            main = Finder.Main;
            levelController = Finder.LevelController;
            mainMenu = GetComponentInParent<MainMenu>();
            
            Button[] buttons = GetComponentsInChildren<Button>();
            returnButton = buttons.WithName(GameObjects.Return);

            //Les listeners sont mis dans unity, ce qui permet de faire une function de OnLevelButtonClicked avec un paramètre
            levelButtons = new List<Button>();
            levelButtons.Add(buttons.WithName(GameObjects.Level1));
            levelButtons.Add(buttons.WithName(GameObjects.Level2));
            levelButtons.Add(buttons.WithName(GameObjects.Level3));
            levelButtons.Add(buttons.WithName(GameObjects.Level4));
            levelButtons.Add(buttons.WithName(GameObjects.Level5));
            levelButtons.Add(buttons.WithName(GameObjects.Level6));
        }

        public void OnEnable()
        {
            returnButton.onClick.AddListener(OnReturnButtonClick);
        }

        public void OnDisable()
        {
            returnButton.onClick.RemoveListener(OnReturnButtonClick);
        }

        protected override void UpdateVisibility()
        {
            gameObject.SetActive(mainMenu.CurrentPage == MainMenuPage.LevelSelectionPage);
        }

        private void OnReturnButtonClick()
        {
            mainMenu.CurrentPage = MainMenuPage.HomePage;
        }
        
        public void OnLevelButtonClicked(Button button)
        {
            for(int i = 0; i < levelButtons.Count; i++)
                if (button.name == levelButtons[i].name)
                {
                    levelController.CurrentLevel = (Level) levelButtons.IndexOf(button);
                    main.GameState = GameState.Game;
                }
        }
    }
}