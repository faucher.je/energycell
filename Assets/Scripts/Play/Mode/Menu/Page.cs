﻿using System;
using UnityEngine;

namespace Game
{
    //Author : Samuel Ménard
    public abstract class Page : MonoBehaviour
    {
        protected MainMenu mainMenu;

        protected void Start()
        {
            mainMenu.OnScreenChanged += OnScreenChanged;
            UpdateVisibility();
        }
        protected void OnDestroy()
        {
            mainMenu.OnScreenChanged -= OnScreenChanged;
        }

        protected void OnScreenChanged()
        {
            UpdateVisibility();
        }
        
        protected virtual void UpdateVisibility()
        {
            throw new NotImplementedException();
        }
        
    }
}