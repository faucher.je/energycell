﻿using Harmony;
using UnityEngine;
using UnityEngine.UI;


namespace Game
{
    //Author : Samuel Ménard
    public class SaveSelectionPage : Page
    {

        private Main main;
        private Button returnButton;
        private Button quitButton;

        private void Awake()
        {
            main = Finder.Main;
            mainMenu = GetComponentInParent<MainMenu>();

            // On prend une array de bouton même s'il n'y en a que peu, du fait que cela va être utile lorsque l'on va
            // faire le vrai menu. 
            Button[] buttons = GetComponentsInChildren<Button>();
            returnButton = buttons.WithName(GameObjects.Save1);
            quitButton = buttons.WithName(GameObjects.Quit);
        }

        public void OnEnable()
        {
            returnButton.onClick.AddListener(OnSave1ButtonClick);
            quitButton.onClick.AddListener(OnQuitButtonClick);
        }
        
        public void OnDisable()
        {
            returnButton.onClick.RemoveListener(OnSave1ButtonClick);
        }
        

        protected override void UpdateVisibility()
        {
            gameObject.SetActive(mainMenu.CurrentPage == MainMenuPage.SaveSelectionPage);
        }
        
        private void OnSave1ButtonClick()
        {
            mainMenu.CurrentPage = MainMenuPage.HomePage;
        }
        
        private void OnQuitButtonClick()
        {
            main.QuitGame();
        }
    }
}