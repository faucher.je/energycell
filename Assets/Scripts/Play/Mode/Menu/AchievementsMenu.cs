﻿using System;
using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    // Author: Jérémy Faucher
    public abstract class AchievementsMenu : MonoBehaviour
    {
        [SerializeField] protected Image[] achievementImages;
        [SerializeField] protected Sprite trophyImage;
        
        private Button quitButton;
        private Button backButton;
        private Button nextButton;
        private AchievementsPage currentPage;
        private AchievementsCollector achievementsCollector;

        public void Awake()
        {
            var buttons = GetComponentsInChildren<Button>();

            quitButton = buttons.WithName(GameObjects.QuitButton);
            backButton = buttons.WithName(GameObjects.BackButton);
            nextButton = buttons.WithName(GameObjects.NextButton);

            var tempPages = GetComponentsInChildren<AchievementsPage>();
            foreach (var page in tempPages)
            {
                page.gameObject.SetActive(false);
            }
            
            currentPage = tempPages.WithName(GameObjects.Page1);
            currentPage.gameObject.SetActive(true);
            
            achievementsCollector = Finder.AchievementsCollector;
            quitButton.onClick.AddListener(OnQuitButtonClick);
            backButton.onClick.AddListener(OnBackButtonClick);
            nextButton.onClick.AddListener(OnNextButtonClick);
        }

        //Author : Samuel Ménard
        private void OnEnable()
        {
            UpdateAchievementImages();
        }

        protected virtual void OnQuitButtonClick()
        {
            //The inherited class needs to change how they leave the page.
            throw new NotImplementedException();
        }

        private void OnBackButtonClick()
        {
            currentPage = currentPage.GoToPreviousPage();
        }

        private void OnNextButtonClick()
        {
            currentPage = currentPage.GoToNextPage();
        }

        // Author : Samuel Ménard
        private void UpdateAchievementImages()
        {
            if (achievementsCollector.StayedLongEnoughInOtherDimension)
                achievementImages[0].sprite = trophyImage;

            if (achievementsCollector.HasKilledBoss)
                achievementImages[1].sprite = trophyImage;

            if (achievementsCollector.HasKilledTurret)
                achievementImages[2].sprite = trophyImage;

            if (achievementsCollector.HasDiedEnoughForAchievement)
                achievementImages[3].sprite = trophyImage;

            if (achievementsCollector.HasReachedMaxHeat)
                achievementImages[4].sprite = trophyImage;

            if (achievementsCollector.HasKilledFleshEnemy)
                achievementImages[5].sprite = trophyImage;

            if (achievementsCollector.HasFinishedGameUnderTimeLimit)
                achievementImages[6].sprite = trophyImage;

            //TODO: Do the verification for the "Trapped" achievement

            if (achievementsCollector.IsLevelsFinished[0])
                achievementImages[8].sprite = trophyImage;

            if (achievementsCollector.IsLevelsFinished[1])
                achievementImages[9].sprite = trophyImage;
            
            if (achievementsCollector.IsLevelsFinished[2])
                achievementImages[10].sprite = trophyImage;
            
            if (achievementsCollector.IsLevelsFinished[3])
                achievementImages[11].sprite = trophyImage;
            
            if (achievementsCollector.IsLevelsFinished[4])
                achievementImages[12].sprite = trophyImage;
            
            if (achievementsCollector.IsLevelsFinished[5])
                achievementImages[13].sprite = trophyImage;
        }
    }
}