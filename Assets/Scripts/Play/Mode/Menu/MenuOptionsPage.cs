﻿namespace Game
{
    //Author: Dave Ouellet
    public class MenuOptionsPage : Options
    {
        private MainMenu mainMenu;
        
        public override void Awake()
        {
            base.Awake();
            mainMenu = GetComponentInParent<MainMenu>();
        }

        private void Start()
        {
            mainMenu.OnScreenChanged += OnScreenChanged;
            UpdateVisibility();
        }
        
        private void OnDestroy()
        {
            mainMenu.OnScreenChanged -= OnScreenChanged;
        }
        
        private void OnScreenChanged()
        {
            UpdateVisibility();
        }

        private void UpdateVisibility()
        {
            gameObject.SetActive(mainMenu.CurrentPage == MainMenuPage.OptionsPage);
        }

        protected override void OnReturnButtonClick()
        {
            mainMenu.CurrentPage = MainMenuPage.HomePage;
        }

        protected override void OnApplyButtonClick()
        {
            base.OnApplyButtonClick();
            mainMenu.CurrentPage = MainMenuPage.HomePage;
        }
        
    }
}