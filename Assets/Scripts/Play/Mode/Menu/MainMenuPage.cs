﻿namespace Game
{
    //Author : Samuel Ménard, Dave Ouellet
    public enum MainMenuPage
    {
        SplashScreenPage,
        SaveSelectionPage,
        LevelSelectionPage,
        HomePage,
        AchievementsPage,
        OptionsPage
    }
}