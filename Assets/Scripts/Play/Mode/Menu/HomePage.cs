using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    //Author : Samuel Ménard, Dave Ouellet
    public class HomePage : MonoBehaviour
    {
        private MainMenu mainMenu;
        private Main main;
        private Button continueButton;
        private Button levelButton;
        private Button achievementButton;
        private Button returnButton;
        private Button optionsButton;
        
        public void Awake()
        {
            mainMenu = GetComponentInParent<MainMenu>();
            main = Finder.Main;
            Button[] buttons = GetComponentsInChildren<Button>();

            continueButton = buttons.WithName(GameObjects.Continue);
            levelButton = buttons.WithName(GameObjects.Level);
            achievementButton = buttons.WithName(GameObjects.Achievements);
            returnButton = buttons.WithName(GameObjects.Return);
            optionsButton = buttons.WithName(GameObjects.Options);
        }
        
        private void Start()
        {
            mainMenu.OnScreenChanged += OnScreenChanged;
            UpdateVisibility();
        }

        public void OnEnable()
        {
            continueButton.onClick.AddListener(OnContinueButtonClick);
            levelButton.onClick.AddListener(OnLevelButtonClick);
            achievementButton.onClick.AddListener(OnAchievementsButtonClick);
            returnButton.onClick.AddListener(OnReturnButtonClick);
            optionsButton.onClick.AddListener(OnOptionsButtonClick);            
        }

        public void OnDisable()
        {
            continueButton.onClick.RemoveListener(OnContinueButtonClick);
            levelButton.onClick.RemoveListener(OnLevelButtonClick);
            achievementButton.onClick.RemoveListener(OnAchievementsButtonClick);
            returnButton.onClick.RemoveListener(OnReturnButtonClick);
            optionsButton.onClick.RemoveListener(OnOptionsButtonClick);
        }

        private void OnDestroy()
        {
            mainMenu.OnScreenChanged -= OnScreenChanged;
        }
        
        private void OnContinueButtonClick()
        {
            main.GameState = GameState.Game;
        }
        
        private void OnLevelButtonClick()
        {
            mainMenu.CurrentPage = MainMenuPage.LevelSelectionPage;
        }
        
        private void OnAchievementsButtonClick()
        {
            mainMenu.CurrentPage = MainMenuPage.AchievementsPage;
        }
        
        private void OnReturnButtonClick()
        {
            mainMenu.CurrentPage = MainMenuPage.SaveSelectionPage;
        }

        private void OnOptionsButtonClick()
        {
            mainMenu.CurrentPage = MainMenuPage.OptionsPage;
        }

        private void UpdateVisibility()
        {
            gameObject.SetActive(mainMenu.CurrentPage == MainMenuPage.HomePage);
        }
        
        private void OnScreenChanged()
        {
            UpdateVisibility();
        }

    }
}