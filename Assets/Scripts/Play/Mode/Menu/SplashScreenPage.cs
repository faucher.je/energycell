﻿using Harmony;
using UnityEngine;

namespace Game
{
    //Author : Samuel Ménard et Jérémy Faucher
    public class SplashScreenPage : Page
    {
        private InputActions.MenuActions playerInput;

        public void Awake()
        {
            playerInput = Finder.Inputs.Actions.Menu;
            mainMenu = GetComponentInParent<MainMenu>();
        }

        private void Update()
        {
            if (playerInput.Submit.triggered)
            {
                mainMenu.CurrentPage = MainMenuPage.SaveSelectionPage;
            }
        }

        protected override void UpdateVisibility()
        {
            gameObject.SetActive(mainMenu.CurrentPage == MainMenuPage.SplashScreenPage);
        }
    }
}