﻿using Harmony;
using UnityEngine;

namespace Game
{
    //Author : Samuel Ménard
    [Findable(Tags.MainMenuController)]
    public class MainMenu : MonoBehaviour
    {
        private MainMenuPage currentPage;
        private MainMenuSettings mainMenuSettings;
        public event MainMenuScreenEvent OnScreenChanged;

        public MainMenuPage CurrentPage
        {
            get => currentPage;
            set
            {
                if (currentPage == value) return;
                currentPage = value;
                if (OnScreenChanged != null) OnScreenChanged();
            }
        }
        
        private void Awake()
        {
            mainMenuSettings = Finder.MainMenuSettings;
            CurrentPage = mainMenuSettings.FirstPageToLoad;
        }
    }
    
    public delegate void MainMenuScreenEvent();
}