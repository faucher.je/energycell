﻿using System;

namespace Game
{
    //Author : Samuel Ménard
    //I wasn't able to think of a way for the class to inherite both Page and AchivementsMenu, so I had to rewrite every
    //Code of Pages in this class
    public class MainMenuAchievements : AchievementsMenu
    {
        private MainMenu mainMenu;
        
        public void Awake()
        {
            base.Awake();
            mainMenu = GetComponentInParent<MainMenu>();
            UpdateVisibility();
            
            mainMenu.OnScreenChanged += OnScreenChanged;
        }
        protected void OnDestroy()
        {
            mainMenu.OnScreenChanged -= OnScreenChanged;
        }

        protected void OnScreenChanged()
        {
            UpdateVisibility();
        }
        
        protected virtual void UpdateVisibility()
        {
            gameObject.SetActive(mainMenu.CurrentPage == MainMenuPage.AchievementsPage);
        }
        protected override void OnQuitButtonClick()
        {
            mainMenu.CurrentPage = MainMenuPage.HomePage;
        }
    }
}