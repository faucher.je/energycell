﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Samuel Ménard
    [Findable(Tags.MainController)]
    public class MainMenuSettings : MonoBehaviour
    {
        
        [SerializeField] private MainMenuPage firstPageToLoad;
        
        public MainMenuPage FirstPageToLoad
        {
            get => firstPageToLoad;
            set => firstPageToLoad = value;
        }
    }
}