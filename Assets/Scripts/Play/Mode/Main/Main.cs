using DG.Tweening;
using Harmony;
using UnityEngine;

namespace Game
{
    [Findable(Tags.MainController)]
    public class Main : MonoBehaviour
    {
        [Header("Scenes")] [SerializeField] private SceneBundle homeScenes;
        [SerializeField] private SceneBundle pauseMenuScenes;
        [SerializeField] private SceneBundle tutorialScenes;
        [SerializeField] private SceneBundle gameEndScenes;
        [SerializeField] private GameState initialGameState;
        
        private GameState gameState;
        private SceneBundleLoader loader;
        private Inputs playerInputs;
        private LevelController levelController;

        //Author : Samuel Ménard, Mickael Chabot, Mathieu Boutet
        public GameState GameState
        {
            get => gameState;
            set
            {
                if (gameState == value) return;
                
                switch (gameState)
                {
                    case GameState.Menu:
                        playerInputs.Actions.Menu.Disable();
                        UnloadHomeScenes();
                        break;
                    case GameState.Game:
                        playerInputs.Actions.Game.Disable();
                        levelController.UnloadCurrentLevel();
                        break;
                }
                switch (value)
                {
                    case GameState.Menu:
                        playerInputs.Actions.Menu.Enable();
                        LoadHomeScenes();
                        break;
                    case GameState.Game:
                        playerInputs.Actions.Game.Enable();
                        levelController.LoadCurrentLevel();
                        break;
                }
                gameState = value;
            }
        }

        private void Awake()
        {
            loader = Finder.SceneBundleLoader;

            DOTween.Init(false, true, LogBehaviour.ErrorsOnly);
            DOTween.SetTweensCapacity(200, 125);
            
            playerInputs = Finder.Inputs;
            levelController = Finder.LevelController;
        }

        private void Start()
        {
            GameState = initialGameState;
        }

        public Coroutine LoadHomeScenes()
        {
#if UNITY_EDITOR
            if (homeScenes.IsLoaded)
                return null;
#endif
            return loader.Load(homeScenes);
        }

        public Coroutine UnloadHomeScenes()
        {
            return loader.Unload(homeScenes);
        }

        // Author: Jérémy Faucher
        public Coroutine LoadPauseMenuScenes()
        {
            return loader.Load(pauseMenuScenes);
        }

        // Author: Jérémy Faucher
        public Coroutine UnloadPauseMenuScenes()
        {
            return loader.Unload(pauseMenuScenes);
        }

        //Author: Dave Ouellet
        public Coroutine LoadTutorialMenuScenes()
        {
            return loader.Load(tutorialScenes);
        }
        
        //Author: Dave Ouellet
        public Coroutine UnLoadTutorialMenuScenes()
        {
            return loader.Unload(tutorialScenes);
        }

        // Author: Jérémy Faucher
        public Coroutine LoadGameEndScenes()
        {
            levelController.UnloadCurrentLevel();
            return loader.Load(gameEndScenes);
        }

        // Author: Jérémy Faucher
        public Coroutine UnloadGameEndScenes()
        {
            return loader.Unload(gameEndScenes);
        }

        public void QuitGame()
        {
            Application.Quit();
        }
    }
}
