﻿using System;
using System.Linq;
using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Mickael Chabot
    [Findable(Tags.MainController)]
    public class LevelController : MonoBehaviour
    {
        [SerializeField] private SceneBundle[] levelScenes;
        [SerializeField] private Level startLevel;
        
        private SceneBundleLoader loader;
        private AchievementSpeedrunnerEventChannel achievementSpeedrunnerEventChannel;
        
        // TODO: A déplacé quand les sauvegardes vont être implémenté
        public Level CurrentLevel { get; set; }
        
        public static int NumberOfLevel =  Enum.GetNames(typeof(Level)).Length;

        private void Awake()
        {
            achievementSpeedrunnerEventChannel = GetComponent<AchievementSpeedrunnerEventChannel>();
            loader = Finder.SceneBundleLoader;
            CurrentLevel = startLevel;
        }

        public Coroutine LoadLevel(Level level)
        {
            if(level == Level.Level1)
                achievementSpeedrunnerEventChannel.Publish(true);
            
#if UNITY_EDITOR
            if (levelScenes[(int)level].IsLoaded) return null;
#endif
            return loader.Load(levelScenes[(int)level]);
        }
        
        public Coroutine LoadCurrentLevel()
        {
            return LoadLevel(CurrentLevel);
        }

        public Coroutine LoadNextLevel()
        {
            UnloadCurrentLevel();
            //On prend la valeur la plus haute dans l'énum et on compare.
            if (NumberOfLevel != (int) CurrentLevel)
            {
                return LoadLevel(++CurrentLevel);
            }
            else
            {
                Debug.Assert(GetLevelEnumLength() < (int)++CurrentLevel);
                return null;
            }
        }
        
        public Coroutine UnloadCurrentLevel()
        {
            return loader.Unload(levelScenes[(int)CurrentLevel]);
        }

        private static int GetLevelEnumLength()
        {
            return Enum.GetNames(typeof(Level)).Length;
        }
    }
}