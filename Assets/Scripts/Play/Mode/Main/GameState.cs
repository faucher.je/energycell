﻿namespace Game
{
    //Author: Samuel Ménard, Mickael Chabot, Mathieu Boutet
    public enum GameState
    {
        None,
        Menu,
        Game
    }
}