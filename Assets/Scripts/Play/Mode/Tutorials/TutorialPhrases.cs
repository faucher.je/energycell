﻿namespace Game
{
    //Author: Dave Ouellet
    public static class TutorialPhrases
    {
        //Heat
        public const string HEAT_TUTORIAL_NAME = "Heat bar tutorial";
        public const string HEAT_TUTORIAL_TEXT = "Your heat bar goes up when using your gadgets. \nWhen your heat bar is full, you lose health if you use your gadgets. \nTo reduce your heat, stop using your gadgets for a while.";
        
        //Taser
        public const string TASER_TUTORIAL_NAME = "Taser tutorial";
        public const string TASER_TUTORIAL_TEXT = "You have acquired a taser.\nYou can now stun organic ennemies and destroy synthetic ones.\nUsing your taser makes you gain heat";

        //Double jump
        public const string DOUBLE_JUMP_TUTORIAL_NAME = "Double jump tutorial";
        public const string DOUBLE_JUMP_TUTORIAL_TEXT = "You have acquired a gadget that enables you to perform a double jump.\nPerforming a double jump makes you gain heat.\nPress the jump key a second time while airborne to double jump.";
        
        //Dimension gadget
        public const string DIMENSION_GADGET_TUTORIAL_NAME = "Dimension change tutorial";
        public const string DIMENSION_GADGET_TUTORIAL_TEXT = "You have acquired a gadget to change between two dimensions. \nPress left shift to switch dimensions. \nYou will gain heat periodically when in the second dimension.";

    }
}