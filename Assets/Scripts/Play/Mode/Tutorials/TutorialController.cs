﻿using Harmony;
using UnityEngine;


namespace Game
{
            
    //Author: Dave Ouellet
    [Findable(Tags.MainController)]
    public class TutorialController : MonoBehaviour
    {
        private PlayerHeatChangeEventChannel playerHeatChangeEventChannel;
        private Main main;
        private bool heatTutorialTriggered = false;
        private bool dimensionGadgetTriggered = false;
        private bool doubleJumpGadgetTriggered = false;
        private bool taserGadgetTriggered = false;

        public bool TutorialsAreEnabled
        {
            get; set;
        }
        
        public TutorialType TutorialType
        {
            get;
            private set;
        }

        private void Awake()
        {
            TutorialsAreEnabled = false;
            main = Finder.Main;
            playerHeatChangeEventChannel = Finder.PlayerHeatChangeEventChannel;
        }

        private void HeatMaxed(float playerHeat)
        {
            if (TutorialsAreEnabled && !heatTutorialTriggered && playerHeat == 100)
            {
                TutorialType = TutorialType.HeatAt100;
                Time.timeScale = 0;
                heatTutorialTriggered = true;
                main.LoadTutorialMenuScenes();
            }
        }

        private void TaserAcquired()
        {
            if (TutorialsAreEnabled && !taserGadgetTriggered)
            {
                //TODO: Déclencher cette méthode quand le joueur récupère le taser
                TutorialType = TutorialType.Taser;
                Time.timeScale = 0;
                taserGadgetTriggered = true;
                main.LoadTutorialMenuScenes();
            }
        }
        
        private void DimensionChangeAcquired()
        {
            if (TutorialsAreEnabled && !dimensionGadgetTriggered)
            {
                //TODO: Déclencher cette méthode quand le joueur récupère le gadget de changement de dimension
                TutorialType = TutorialType.DimensionGadget;
                Time.timeScale = 0;
                dimensionGadgetTriggered = true;
                main.LoadTutorialMenuScenes();
            }
        }
        
        private void DoubleJumpAcquired()
        {
            if (TutorialsAreEnabled && !doubleJumpGadgetTriggered)
            {
                //TODO: Déclencher cette méthode quand le joueur récupère le double jump
                TutorialType = TutorialType.DoubleJump;
                Time.timeScale = 0;
                doubleJumpGadgetTriggered = true;
                main.LoadTutorialMenuScenes();
            }
        }


        private void OnEnable()
        {
            playerHeatChangeEventChannel.OnPlayerHeatChange += HeatMaxed;
        }

        private void OnDisable()
        {
            playerHeatChangeEventChannel.OnPlayerHeatChange -= HeatMaxed;
        }
        
        private void OnResumeButtonClick()
        {
            main.UnLoadTutorialMenuScenes();
        }
        
        
    }
}