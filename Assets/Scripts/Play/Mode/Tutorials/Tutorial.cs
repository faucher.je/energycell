﻿using System;
using Harmony;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    //Author: Dave Ouellet
    public class Tutorial : MonoBehaviour
    {
        private Button resumeButton;
        private TextMeshProUGUI tutorialTitle;
        private TextMeshProUGUI tutorialText;
        private Inputs inputs;
        private Main main;
        private TutorialController tutorialController;

        private void Awake()
        {
            main = Finder.Main;
            var texts = GetComponentsInChildren<TextMeshProUGUI>();
            tutorialTitle = texts.WithName(GameObjects.TutorialName);
            tutorialText = texts.WithName(GameObjects.TutorialText);
            var buttons = GetComponentsInChildren<Button>();
            resumeButton = buttons.WithName(GameObjects.Resume);
            inputs = Finder.Inputs;
            tutorialController = Finder.TutorialController;

            switch (tutorialController.TutorialType)
            {
                case TutorialType.HeatAt100:
                {
                    tutorialTitle.SetText(TutorialPhrases.HEAT_TUTORIAL_NAME);
                    tutorialText.SetText(TutorialPhrases.HEAT_TUTORIAL_TEXT);
                    break;
                }
                
                case TutorialType.DoubleJump:
                {
                    tutorialTitle.SetText(TutorialPhrases.DOUBLE_JUMP_TUTORIAL_NAME);
                    tutorialText.SetText(TutorialPhrases.DOUBLE_JUMP_TUTORIAL_TEXT);
                    break;
                }
                
                case TutorialType.Taser:
                {
                    tutorialTitle.SetText(TutorialPhrases.TASER_TUTORIAL_NAME);
                    tutorialText.SetText(TutorialPhrases.TASER_TUTORIAL_TEXT);
                    break;
                }
                
                case TutorialType.DimensionGadget:
                {
                    tutorialTitle.SetText(TutorialPhrases.DIMENSION_GADGET_TUTORIAL_NAME);
                    tutorialText.SetText(TutorialPhrases.DIMENSION_GADGET_TUTORIAL_TEXT);
                    break;
                }
            }
            
        }
        
        private void OnEnable()
        {
            inputs.Actions.Game.Disable();
            inputs.Actions.Menu.Enable();
            resumeButton.onClick.AddListener(OnResumeButtonClick);
        }
        
        private void OnDisable()
        {
            inputs.Actions.Menu.Disable();
            resumeButton.onClick.RemoveListener(OnResumeButtonClick);
        }
        
        private void OnResumeButtonClick()
        {
            inputs.Actions.Game.Enable();
            Time.timeScale = 1;
            main.UnLoadTutorialMenuScenes();
        }
    }
}