﻿namespace Game
{
    //Author: Dave Ouellet
    public enum TutorialType
    {
        HeatAt100,
        DimensionGadget,
        DoubleJump,
        Taser
    }
}