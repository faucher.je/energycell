﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Samuel Ménard
    [Findable(Tags.MainController)]
    public class AchievementSpeedrunnerEventChannel : MonoBehaviour
    {
        public event AchievementSpeedrunnerStateEvent OnAchievementSpeedrunnerStateChanged;

        public void Publish(bool isPlayerDoingAchievement)
        {
            OnAchievementSpeedrunnerStateChanged?.Invoke(isPlayerDoingAchievement);
        }
    }

    public delegate void AchievementSpeedrunnerStateEvent(bool isPlayerDoingAchievement);
}