﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Mickael Chabot
    [Findable(Tags.MainController)]
    public class PlayerHealthChangeEventChannel : MonoBehaviour
    {
        public event PlayerHealthChangedEvent OnPlayerHealthChanged;

        public void Publish(float playerHealth)
        {
            OnPlayerHealthChanged?.Invoke(playerHealth);
        }
    }

    public delegate void PlayerHealthChangedEvent(float playerHealth);
}