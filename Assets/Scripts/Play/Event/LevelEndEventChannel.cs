﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Samuel Ménard
    [Findable(Tags.MainController)]
    public class LevelEndEventChannel : MonoBehaviour
    {
        public event LevelEndEvent OnLevelEnd;

        public void Publish(Level level) 
        { 
            OnLevelEnd?.Invoke(level);
        }
    }
    public delegate void LevelEndEvent(Level level);
}