﻿using Harmony;
using UnityEngine;

namespace Game
{
    //Author: Jérémy Faucher
    [Findable(Tags.MainController)]
    public class BossHitEventChannel : MonoBehaviour
    {
        public event BossHitEvent OnBossHit;

        public void Publish(float health)
        {
            OnBossHit?.Invoke(health);
        }
    }

    public delegate void BossHitEvent(float health);
}