﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Dave Ouellet
    [Findable(Tags.MainController)]
    public class DimensionChangeEventChannel : MonoBehaviour
    {
        public event DimensionChangeEvent OnDimensionChange;

        public void Publish(Dimension dimension)
        {
            OnDimensionChange?.Invoke(dimension);
        }
    }

    public delegate void DimensionChangeEvent(Dimension dimension);
}