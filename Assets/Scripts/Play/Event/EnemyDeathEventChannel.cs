﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Samuel Ménard
    [Findable(Tags.MainController)]
    public class EnemyDeathEventChannel : MonoBehaviour
    {
        public event EnemyDeathEvent OnEnemyDeath;

        public void Publish(EnemyType enemyType)
        {
            OnEnemyDeath?.Invoke(enemyType);
        }
    }

    public delegate void EnemyDeathEvent(EnemyType enemyType);
}