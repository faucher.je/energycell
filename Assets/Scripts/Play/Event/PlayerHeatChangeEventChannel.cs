﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Mickael Chabot
    [Findable(Tags.MainController)]
    public class PlayerHeatChangeEventChannel : MonoBehaviour
    {
        public event PlayerHeatChangeEvent OnPlayerHeatChange;

        public void Publish(float playerHeat)
        {
            OnPlayerHeatChange?.Invoke(playerHeat);
        }
    }

    public delegate void PlayerHeatChangeEvent(float playerHeat);
}