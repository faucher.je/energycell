﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Samuel Ménard
    [Findable(Tags.MainController)]
    public class AchievementObtainedEventChannel : MonoBehaviour
    {
        public event AchievementObtainedEvent OnAchievementGained;

        public void Publish(Achievement achievement)
        {
            OnAchievementGained?.Invoke(achievement);
        }
    }

    public delegate void AchievementObtainedEvent(Achievement achievement);
}