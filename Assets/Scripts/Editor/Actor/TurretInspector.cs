using Harmony;
using UnityEditor;

namespace Game
{
    // Author : Mathieu Boutet
    [CustomEditor(typeof(Turret), true)]
    public class TurretInspector : BaseInspector
    {
        private Turret turret;

        protected override void Initialize()
        {
            turret = target as Turret;
        }

        protected override void Draw()
        {
            Initialize();
            DrawDefault();
            
            if (EditorApplication.isPlaying && turret != null) //Check if playing and is not prefab
            {
                DrawStats();
            }
            else
            {
                DrawWarningBox("Turret information can only be seen on real turret (not prefab) and only in play mode.");
            }
        }

        private void DrawStats()
        {
            BeginTable();
            BeginTableCol();
            BeginTableRow();
            DrawTableHeader("Stats");
            EndTableRow();
            BeginTableRow();
            DrawTableCell("Standby aim rotation: " + turret.StandbyAimRotation.eulerAngles);
            EndTableRow();
            BeginTableRow();
            DrawTableCell("Target aim rotation: " + turret.TargetAimRotation.eulerAngles);
            EndTableRow();
            BeginTableRow();
            DrawTableCell("Local rotation: " + turret.HeadTransform.localRotation.eulerAngles);
            EndTableRow();
            BeginTableRow();
            DrawTableCell("Global rotation: " + turret.HeadTransform.rotation.eulerAngles);
            EndTableRow();
            BeginTableRow();
            DrawTableCell("Target spotted: " + turret.TargetSpotted);
            EndTableRow();
            BeginTableRow();
            DrawTableCell("Target aim rotation reached: " + turret.TargetAimRotationReached);
            EndTableRow();
            BeginTableRow();
            DrawTableCell("Locked on target: " + turret.LockedOnTarget);
            EndTableRow();
            EndTableCol();
            EndTable();
        }
    }
}